import HeaderLayout from './view/components/header-layout';
import FooterLayout from './view/components/footer-layout';
import LoginPage from './view/pages/login-page';
import VehiclesPage from './view/pages/vehicles-page';
import VehicleSleepPage from './view/pages/vehicle-sleep-page';


export default (store) => {
    return {
        '/login': {
            view: () => [m(HeaderLayout, { store }), m(LoginPage, { store }), m(FooterLayout)],
        },
        '/vehicles': {
            view: () => [m(HeaderLayout, { store }), m(VehiclesPage, { store }), m(FooterLayout)],
        },
        '/vehicles/:id/sleep': {
            onmatch(args) {
                return {
                    view: () => {
                        return [m(HeaderLayout, { store }), m(VehicleSleepPage, { store, id: args.id }), m(FooterLayout)];
                    },
                };
            },
        },
    };
};