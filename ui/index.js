import store from './store';
import routes from './routes';
import './app.scss';
import './libs/translate';

window.onload = () => {
    m.route.prefix('');

    // i18n
    store.lang.set().then(() => {
        store.users.init();
        m.route(document.getElementById('app'), store.users.data.firstName ? '/vehicles' : '/login', routes(store));
    });
};