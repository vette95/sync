import './styles.scss';
export default {
    view() {
        return (
            <footer>
                <div class="container">
                    <div class="columns top">
                        <div class="container grid-lg">
                            <div class="column col-md-12 col-6 col-mr-auto">
                                <h5 class="text-light">{mx.translate('footer.about')}</h5>
                                <span class="text-gray">{mx.translate('footer.text')}</span>
                            </div>
                        </div>
                    </div>
                    <div class="columns bottom">
                        <div class="container grid-lg">
                            <div class="columns">
                                <div class="column col-xs-12 col-6 text-gray">
                                    © 2019 <a href="/" oncreate={m.route.link} class="link">tronity.io</a>
                                </div>
                                <div class="column col-xs-12 col-6 text-right">
                                    <a class="mr-3 text-gray" href="https://tronity.io/privacy" target="_blank" rel="noopener noreferrer">{mx.translate('footer.privacy')}</a>
                                    <a class="mr-3 text-gray" href="https://tronity.io/contact" target="_blank" rel="noopener noreferrer">{mx.translate('footer.contact')}</a>
                                    <a class="text-gray" href="https://tronity.io/impressum" target="_blank" rel="noopener noreferrer">{mx.translate('footer.impressum')}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    },
};