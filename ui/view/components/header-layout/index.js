import './styles.scss';
export default {
    store: null,
    oninit(vnode) {
        this.store = vnode.attrs.store;
    },
    signOut() {
        this.store.users.logout();
    },
    changeLang(value) {
        this.store.lang.set(value);
    },
    active(pathname) {
        if (location.pathname.search(pathname) !== -1) {
            return 'active';
        }
        return '';
    },
    view(vnode) {
        return (
            <header>
                <div class="container">
                    <div class="container grid-lg columns header">
                        <div class="column col-6">
                            <a href="/" oncreate={m.route.link} class="navbar-brand mr-4"><span class="text-primary"><b>tronity.io</b></span> <span class="text-gray hide-md">| {mx.translate('header.text')}</span></a>
                        </div>
                        <div class="column col-6 text-gray user">
                            {
                                vnode.attrs.store.users.data.firstName &&
                                <span class="hide-xs">{vnode.attrs.store.users.data.firstName} {vnode.attrs.store.users.data.lastName} |</span>
                            }
                            {
                                vnode.attrs.store.users.data.firstName ? (
                                    <button class="btn btn-link text-gray" onclick={() => { vnode.state.signOut(); }}>
                                        {mx.translate('header.logout')}
                                    </button>
                                ) : (
                                    <a class="btn btn-link text-gray" href="/login" oncreate={m.route.link}>
                                        {mx.translate('header.login')}
                                    </a>
                                )
                            }
                            <div>
                                | <div class="dropdown dropdown-right">
                                    <button class="btn btn-link dropdown-toggle text-gray">
                                        {vnode.attrs.store.lang.value.toUpperCase()} <i class="icon icon-caret"></i>
                                    </button>
                                    <ul id="lang" class="menu text-center">
                                        <li class="menu-item" onclick={() => vnode.state.changeLang('de')}>
                                            DE
                                        </li>
                                        <li class="menu-item" onclick={() => vnode.state.changeLang('en')}>
                                            EN
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="columns navigation">
                        <div class="container grid-lg">
                            <div class="column col-xs-12">
                                {
                                    vnode.attrs.store.users.data.firstName ? (
                                        <div class="d-flex">
                                            <a href="/vehicles" oncreate={m.route.link} className={`btn btn-link ${this.active('/vehicles')}`}>
                                                {mx.translate('vehicle')}
                                            </a>
                                            <div class="info ml-2 mt-1"> <b>{mx.translate('header.private')}</b> {mx.translate('header.deployment')}</div>
                                        </div>
                                    ) : (
                                        <div class="info"><b>{mx.translate('header.private')}</b> {mx.translate('header.deployment')}</div>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {
                    vnode.attrs.store.toast.show &&
                    <div class={`toast toast-${vnode.attrs.store.toast.type} mb-4`}>
                        <button class="btn btn-clear float-right" onclick={() => { vnode.attrs.store.toast.close(); }} />
                        {vnode.attrs.store.toast.message}
                    </div>
                }
            </header>
        );
    },
};