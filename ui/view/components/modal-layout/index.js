import './styles.scss';
export default {
    close(e, id) {
        e.preventDefault();
        document.getElementById(id).classList.remove('active');
    },
    view(vnode) {
        return (
            <div class="modal" id={vnode.attrs.id}>
                <a href="" class="modal-overlay" aria-label="Close" onclick={(e) => { vnode.state.close(e, vnode.attrs.id); }}></a>
                <div class="modal-container">
                    <div class="modal-header">
                        <a href="" class="btn btn-clear float-right" aria-label="Close"
                            onclick={(e) => { vnode.state.close(e, vnode.attrs.id); }}
                        ></a>
                        <div class="modal-title h5">
                            {vnode.attrs.title}
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="content">
                            {vnode.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    },
};