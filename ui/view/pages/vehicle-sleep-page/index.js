import './styles.scss';

export default {
    loading: false,
    loadingSave: false,
    vehicle: {},
    oninit(vnode) {
        this.id = vnode.attrs.id;
        this.store = vnode.attrs.store;
        this.loading = true;
        this.store.vehicles.get(this.id).then(vehicle => {
            this.vehicle = vehicle;
            this.loading = false;
            m.redraw();
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.error.load'));
            this.loading = false;
            m.redraw();
        });
    },
    flex(e) {
        if (e.target.checked) {
            this.vehicle.flex = true;
            this.vehicle.predict = true;
            this.vehicle.manual = false;
        } else {
            this.vehicle.flex = false;
            this.vehicle.predict = false;
            this.vehicle.manual = false;
        }
        m.redraw();
    },
    save() {
        this.store.vehicles.patch(this.id, {
            flex: document.getElementById('flex').checked,
            predict: document.getElementById('predict').checked,
            manual: document.getElementById('manual').checked,
            timeStartSleep: document.getElementById('timeStartSleep').value,
            timeWaitSleep: document.getElementById('timeWaitSleep').value,
            fix: document.getElementById('fix').checked,
            time: {
                Mon: {
                    start: document.getElementById('mos').value,
                    end: document.getElementById('moe').value,
                },
                Tue: {
                    start: document.getElementById('dis').value,
                    end: document.getElementById('die').value,
                },
                Wed: {
                    start: document.getElementById('mis').value,
                    end: document.getElementById('mie').value,
                },
                Thu: {
                    start: document.getElementById('dos').value,
                    end: document.getElementById('doe').value,
                },
                Fri: {
                    start: document.getElementById('frs').value,
                    end: document.getElementById('fre').value,
                },
                Sat: {
                    start: document.getElementById('sas').value,
                    end: document.getElementById('sae').value,
                },
                Sun: {
                    start: document.getElementById('sos').value,
                    end: document.getElementById('soe').value,
                },
            },
            wakeUp: document.getElementById('wakeUp').checked,
            wakeUpTime: {
                Mon: {
                    start: document.getElementById('wmos').value,
                    end: document.getElementById('wmoe').value,
                },
                Tue: {
                    start: document.getElementById('wdis').value,
                    end: document.getElementById('wdie').value,
                },
                Wed: {
                    start: document.getElementById('wmis').value,
                    end: document.getElementById('wmie').value,
                },
                Thu: {
                    start: document.getElementById('wdos').value,
                    end: document.getElementById('wdoe').value,
                },
                Fri: {
                    start: document.getElementById('wfrs').value,
                    end: document.getElementById('wfre').value,
                },
                Sat: {
                    start: document.getElementById('wsas').value,
                    end: document.getElementById('wsae').value,
                },
                Sun: {
                    start: document.getElementById('wsos').value,
                    end: document.getElementById('wsoe').value,
                },
            },
        }).then(() => {
            this.store.toast.showMessage('success', mx.translate('idle.message'));
            window.history.back();
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.error.patch'));
        });
    },
    manual(e) {
        this.vehicle.manual = e.target.checked;
        if (!e.target.checked) {
            this.vehicle.timeStartSleep = '';
            this.vehicle.timeWaitSleep = '';
        }
    },
    fix(e) {
        this.vehicle.fix = e.target.checked;
        if (!e.target.checked) {
            this.vehicle.time = null;
        }
    },
    wakeUp(e) {
        this.vehicle.wakeUp = e.target.checked;
        if (!e.target.checked) {
            this.vehicle.wakeUpTime = null;
        }
    },
    view(vnode) {
        return (
            <main class="container grid-lg vehicle-sleep" >
                {
                    vnode.state.loading &&
                    <div class="text-center">
                        <div class="loading loading-lg mb-2"></div>
                        {mx.translate('loading')}
                    </div>
                }
                {
                    !vnode.state.loading &&
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-gray h5 ">
                                <div class="columns">
                                    <div class="column col-xs-12 col-6">
                                        {mx.translate('idle.header')}
                                    </div>
                                    <div class="column col-xs-12 col-6 text-right">
                                        <div class="dropdown dropdown-right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="divider"></div>
                        </div>
                        <div class="card-body">
                            <label class="form-switch">
                                <input type="checkbox" checked={vnode.state.vehicle.flex} id="flex" onchange={(e) => { vnode.state.flex(e); }} />
                                <i class="form-icon"></i>{mx.translate('idle.flex')}
                            </label>
                            {mx.translate('idle.flexText')}
                            <div class="divider" />
                            <label class="form-switch">
                                <input type="checkbox" checked={vnode.state.vehicle.predict} id="predict" disabled={!vnode.state.vehicle.flex} />
                                <i class="form-icon"></i>{mx.translate('idle.weakup')}
                            </label>
                            {mx.translate('idle.weakupText')}
                            <div class="divider" />
                            <div class="mt-2 mb-2">
                                {mx.translate('vehicles.manuelWakeUp')}<br />
                                {mx.translate('vehicles.manuelWakeUpText1')}:<br />
                                <div class="mt-2 mb-2 text-bold">{`https://tronity.io/wakeup/${vnode.state.vehicle._id}`}</div>
                                {mx.translate('vehicles.manuelWakeUpText2')}
                            </div>
                            <div class="divider" />
                            <label class="form-switch mt-4">
                                <input type="checkbox" checked={vnode.state.vehicle.manual} id="manual" disabled={!vnode.state.vehicle.flex} onchange={(e) => {
                                    vnode.state.manual(e);
                                }} />
                                <i class="form-icon"></i>{mx.translate('idle.manual')}
                            </label>
                            <div class="form-group">
                                <label class="form-label">{mx.translate('idle.manualText1')}:</label>
                                <input class="form-input" type="text" id="timeStartSleep" disabled={!vnode.state.vehicle.manual}
                                    value={vnode.state.vehicle.timeStartSleep} placeholder="e.g. 60" />
                            </div>
                            <div class="form-group">
                                <label class="form-label">{mx.translate('idle.manualText2')}:</label>
                                <input class="form-input" type="text" id="timeWaitSleep" disabled={!vnode.state.vehicle.manual}
                                    value={vnode.state.vehicle.timeWaitSleep} placeholder="e.g. 20" />
                            </div>
                            <div class="divider mt-4" />
                            <label class="form-switch mt-4">
                                <input type="checkbox" checked={vnode.state.vehicle.fix} id="fix" onchange={(e) => {
                                    vnode.state.fix(e);
                                }} />
                                <i class="form-icon"></i>{mx.translate('idle.fix')}
                            </label>
                            {mx.translate('idle.fixText')}
                            <table class="table mb-2">
                                <thead>
                                    <tr>
                                        <th>Day</th>
                                        <th>Start</th>
                                        <th>End</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.mo')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="mos" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Mon.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="moe" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Mon.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.th')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="dis" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Tue.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="die" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Tue.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.we')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="mis" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Wed.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="mie" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Wed.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.th')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="dos" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Thu.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="doe" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Thu.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.fr')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="frs" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Fri.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="fre" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Fri.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.sa')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="sas" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Sat.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="sae" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Sat.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.su')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="sos" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Sun.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.fix} class="form-input" type="text" id="soe" value={vnode.state.vehicle.time && vnode.state.vehicle.time.Sun.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="divider mt-4" />
                            <label class="form-switch mt-4">
                                <input type="checkbox" checked={vnode.state.vehicle.wakeUp} id="wakeUp" onchange={(e) => {
                                    vnode.state.wakeUp(e);
                                }} />
                                <i class="form-icon"></i>{mx.translate('idle.wakeUp')}
                            </label>
                            {mx.translate('idle.wakeUpText')}
                            <table class="table mb-2">
                                <thead>
                                    <tr>
                                        <th>Day</th>
                                        <th>Start</th>
                                        <th>End</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.mo')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wmos" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Mon.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wmoe" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Mon.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.tu')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wdis" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Tue.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wdie" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Tue.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.we')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wmis" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Wed.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wmie" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Wed.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.th')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wdos" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Thu.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wdoe" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Thu.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.fr')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wfrs" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Fri.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wfre" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Fri.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.sa')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wsas" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Sat.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wsae" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Sat.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{mx.translate('vehicles.time.su')}</td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wsos" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Sun.start} placeholder="23:00" />
                                        </td>
                                        <td>
                                            <input disabled={!vnode.state.vehicle.wakeUp} class="form-input" type="text" id="wsoe" value={vnode.state.vehicle.wakeUpTime && vnode.state.vehicle.wakeUpTime.Sun.end} placeholder="06:00" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button class={'btn btn mt-2'}
                                onclick={() => { window.history.back(); }}>{mx.translate('cancel')}</button>
                            <button class={`btn btn-primary mt-2 ml-2 ${vnode.state.loadingSave && 'loading'}`}
                                onclick={() => { vnode.state.save(); }}>{mx.translate('save')}</button>
                        </div>
                    </div>
                }
            </main >
        );
    },
};