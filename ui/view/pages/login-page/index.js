import './styles.scss';

export default {
    loading: false,
    oninit(vnode) {
        this.store = vnode.attrs.store;
        this.data = {
            strategy: 'local',
            password: null,
            token: null,
            email: null,
        };
    },
    login() {
        this.loading = true;
        this.store.users.login(this.data).then(() => {
            this.loading = false;
            m.route.set('/vehicles');
        }).catch((e) => {
            if (e.message === 'token') {
                this.token = true;
                this.loading = false;
                m.redraw();
            } else {
                this.store.toast.showMessage('error', mx.translate('login.error'));
                this.loading = false;
            }
        });
    },
    view(vnode) {
        return (
            <main class="container grid-lg login text-center">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title text-gray h5 ">
                            <b>{mx.translate('login.login')}</b>
                        </div>
                        <div class="divider"></div>
                    </div>
                    <div class="card-body">
                        <div class="mb-2">
                            {mx.translate('login.message')}
                        </div>
                        {
                            !this.token ? (
                                <div class="pt-2">
                                    <div class="form-group">
                                        <input class="form-input" type="email"
                                            placeholder={mx.translate('login.email')}
                                            value={vnode.state.data.email}
                                            oninput={(e) => { vnode.state.data.email = e.target.value; }}
                                        />
                                    </div>
                                    <div class="form-group">
                                        <input class="form-input" type="password"
                                            placeholder={mx.translate('login.password')}
                                            value={vnode.state.data.password}
                                            oninput={(e) => { vnode.state.data.password = e.target.value; }}
                                            onkeydown={(e) => { e.key === 'Enter' && vnode.state.login(); }}
                                        />
                                    </div>
                                    <button class={`btn btn-primary mb-2 ${vnode.state.loading && 'loading'}`}
                                        onclick={() => { vnode.state.login(); }}>{mx.translate('login.login')}</button>
                                    <p class="text-right mt-2">
                                        <span class="mr-2">{mx.translate('login.new')}</span>
                                        <a href="https://www.tronity.io" target="_blank" rel="noopener noreferrer">{mx.translate('login.start')}</a>
                                    </p>
                                </div>
                            ) : (
                                <div class="text-center">
                                    {mx.translate('login.text')}
                                    <div class="form-group mt-2">
                                        <input class="form-input" type="text"
                                            value={vnode.state.data.token}
                                            oninput={(e) => { vnode.state.data.token = e.target.value; }}
                                            onkeydown={(e) => { e.key === 'Enter' && vnode.state.login(); }}
                                        />
                                    </div>
                                    <button class={`btn btn-primary ${vnode.state.loading && 'loading'}`}
                                        onclick={() => { vnode.state.login(); }}>{mx.translate('login.verify')}</button>
                                </div>
                            )
                        }

                    </div>
                </div>
            </main>
        );
    },
};