import ModalLayout from '../../../components/modal-layout';

export default {
    sleep: null,
    loading: false,
    oninit(vnode) {
        this.store = vnode.attrs.store;
        this.reload = vnode.attrs.reload;
    },
    save(vehicle) {
        this.store.vehicles.patch(vehicle._id, {
            sleep: document.getElementById('sleep').value === 'on' ? true : false,
            timeStartSleep: document.getElementById('timeStartSleep').value,
            timeWaitSleep: document.getElementById('timeWaitSleep').value,
            time: {
                Mon: {
                    start: document.getElementById('mos').value,
                    end: document.getElementById('moe').value,
                },
                Tue: {
                    start: document.getElementById('dis').value,
                    end: document.getElementById('die').value,
                },
                Wed: {
                    start: document.getElementById('mis').value,
                    end: document.getElementById('mie').value,
                },
                Thu: {
                    start: document.getElementById('dos').value,
                    end: document.getElementById('doe').value,
                },
                Fri: {
                    start: document.getElementById('frs').value,
                    end: document.getElementById('fre').value,
                },
                Sat: {
                    start: document.getElementById('sas').value,
                    end: document.getElementById('sae').value,
                },
                Sun: {
                    start: document.getElementById('sos').value,
                    end: document.getElementById('soe').value,
                },
            },
        }).then(() => {
            this.reload();
            document.getElementById('time').classList.remove('active');
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.error.patch'));
        });
    },
    update(e) {
        if (e.target.checked) {
            this.sleep = true;
        } else {
            this.sleep = false;
            document.getElementById('timeStartSleep').value = '';
            document.getElementById('timeWaitSleep').value = '';
        }
    },
    view(vnode) {
        let sleep = vnode.state.sleep;
        if (sleep === null) {
            sleep = vnode.attrs.vehicle.sleep;
        }

        return (
            <ModalLayout id="time" title={mx.translate('vehicles.sleepInterval')}>
                <h5>{mx.translate('autoSleep')}</h5>
                <label class="form-switch">
                    <input type="checkbox" checked={sleep} id="sleep" onchange={(e) => { vnode.state.update(e); }} />
                    <i class="form-icon"></i>{mx.translate('enable')}
                </label>
                <div class="form-group">
                    <label class="form-label">{mx.translate('timeStartSleep')}:</label>
                    <input class="form-input" type="text" id="timeStartSleep" disabled={sleep ? '' : 'disabled'}
                        value={vnode.attrs.vehicle.timeStartSleep} placeholder="e.g. 30" />
                </div>
                <div class="form-group">
                    <label class="form-label">{mx.translate('timeWaitSleep')}:</label>
                    <input class="form-input" type="text" id="timeWaitSleep" disabled={sleep ? '' : 'disabled'}
                        value={vnode.attrs.vehicle.timeWaitSleep} placeholder="e.g. 30" />
                </div>
                <br />
                <h5>{mx.translate('manuelSleep')}</h5>
                <table class="table mb-2">
                    <thead>
                        <tr>
                            <th>Day</th>
                            <th>Start</th>
                            <th>End</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Mo</td>
                            <td>
                                <input class="form-input" type="text" id="mos" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Mon.start} placeholder="23:00" />
                            </td>
                            <td>
                                <input class="form-input" type="text" id="moe" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Mon.end} placeholder="06:00" />
                            </td>
                        </tr>
                        <tr>
                            <td>Di</td>
                            <td>
                                <input class="form-input" type="text" id="dis" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Tue.start} placeholder="23:00" />
                            </td>
                            <td>
                                <input class="form-input" type="text" id="die" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Tue.end} placeholder="06:00" />
                            </td>
                        </tr>
                        <tr>
                            <td>Mi</td>
                            <td>
                                <input class="form-input" type="text" id="mis" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Wed.start} placeholder="23:00" />
                            </td>
                            <td>
                                <input class="form-input" type="text" id="mie" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Wed.end} placeholder="06:00" />
                            </td>
                        </tr>
                        <tr>
                            <td>Do</td>
                            <td>
                                <input class="form-input" type="text" id="dos" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Thu.start} placeholder="23:00" />
                            </td>
                            <td>
                                <input class="form-input" type="text" id="doe" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Thu.end} placeholder="06:00" />
                            </td>
                        </tr>
                        <tr>
                            <td>Fr</td>
                            <td>
                                <input class="form-input" type="text" id="frs" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Fri.start} placeholder="23:00" />
                            </td>
                            <td>
                                <input class="form-input" type="text" id="fre" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Fri.end} placeholder="06:00" />
                            </td>
                        </tr>
                        <tr>
                            <td>Sa</td>
                            <td>
                                <input class="form-input" type="text" id="sas" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Sat.start} placeholder="23:00" />
                            </td>
                            <td>
                                <input class="form-input" type="text" id="sae" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Sat.end} placeholder="06:00" />
                            </td>
                        </tr>
                        <tr>
                            <td>So</td>
                            <td>
                                <input class="form-input" type="text" id="sos" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Sun.start} placeholder="23:00" />
                            </td>
                            <td>
                                <input class="form-input" type="text" id="soe" value={vnode.attrs.vehicle.time && vnode.attrs.vehicle.time.Sun.end} placeholder="06:00" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button class={`btn btn-primary mt-2 ${vnode.state.loading && 'loading'}`}
                    onclick={() => { vnode.state.save(vnode.attrs.vehicle); }}>{mx.translate('save')}</button>
            </ModalLayout >
        );
    },
};