import powerform from 'powerform';
import { required, pattern } from 'validatex';
import ModalLayout from '../../../components/modal-layout';

export default {
    loading: false,
    oninit(vnode) {
        this.store = vnode.attrs.store;
        this.reload = vnode.attrs.reload;
        this.form = powerform({
            confirm: {
                default: '',
                validator: [
                    required(true),
                    pattern(new RegExp(/DELETE/)),
                ],
            },
        });
    },
    del(id) {
        if (!this.form.isValid()) {
            return this.error = true;
        }
        this.loading = true;
        this.store.vehicles.del(id).then(() => {
            this.loading = false;
            this.error = false;
            this.form.reset();
            document.getElementById('del').classList.remove('active');
            this.reload();
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.delete.error'));
            this.loading = false;
        });
    },
    view(vnode) {
        let { confirm } = vnode.state.form;
        return (
            <ModalLayout
                id="del"
                title={mx.translate('vehicles.delete.delete')}
                loading={vnode.state.loading}
                click={() => { vnode.state.del(vnode.attrs.id); }}
            >
                <p class="text-error">
                    {mx.translate('vehicles.delete.text')}
                </p>
                <div class={`form-group ${!vnode.state.error || !confirm.isValid() && 'has-error'}`}>
                    <input class="form-input" type="text" placeholder="DELETE"
                        value={confirm()}
                        oninput={e => vnode.state.error ? confirm.setAndValidate(e.target.value) : confirm(e.target.value)}
                    />
                    <p class="form-input-hint">
                        {confirm.error()}
                    </p>
                </div>
                <button class={`btn btn-primary mt-2 ${vnode.state.loading && 'loading'}`}
                    onclick={() => { vnode.state.del(vnode.attrs.id); }}>{mx.translate('confirm')}</button>
            </ModalLayout>
        );
    },
};