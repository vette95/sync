import powerform from 'powerform';
import { required, isString } from 'validatex';
import ModalLayout from '../../../components/modal-layout';

export default {
    loading: false,
    oninit(vnode) {
        this.error = false;
        this.store = vnode.attrs.store;
        this.reload = vnode.attrs.reload;
        this.form = powerform({
            manufacture: {
                default: '',
                validator: required(true),
            },
            email: {
                default: '',
                validator: [
                    isString(),
                ],
            },
            password: {
                default: '',
                validator: isString(),
            },
            token: {
                default: '',
                validator: isString(),
            },
        });
    },
    login() {
        if (!this.form.isValid()) {
            return this.error = true;
        }

        this.loading = true;
        this.store.vehicles.validate({
            manufacture: this.form.manufacture(),
            email: this.form.email(),
            password: this.form.password(),
            token: this.form.token(),
        }).then((data) => {
            this.loading = false;
            if (data.length === 0) {
                this.store.toast.showMessage('error', mx.translate('vehicles.add.error.no'));
                return document.getElementById('add').classList.remove('active');
            }
            this.vehicles = data;
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.add.error.login'));
            this.loading = false;
        });
    },
    add(id) {
        const vehicle = this.vehicles.find((elm) => {
            return elm.vehicleId === id;
        });
        this.loading = true;
        this.store.vehicles.add({
            manufacture: this.form.manufacture(),
            email: this.form.email(),
            password: this.form.password(),
            token: this.form.token(),
            vehicleId: vehicle.vehicleId,
            displayName: vehicle.displayName,
            model: vehicle.model,
            vin: vehicle.vin,
            kWh: vehicle.kWh,
        }).then(() => {
            this.vehicles = undefined;
            this.loading = false;
            this.form.reset();
            document.getElementById('add').classList.remove('active');
            this.reload();
        }).catch((e) => {
            if (e.code === 408) {
                this.store.toast.showMessage('error', mx.translate('vehicles.add.error.not'));
            } else {
                this.store.toast.showMessage('error', mx.translate('vehicles.add.error.add'));
            }
            this.loading = false;
        });
    },
    view(vnode) {
        let { manufacture, email, password, token } = vnode.state.form;
        return (
            <ModalLayout id="add" title={mx.translate('vehicles.add-vehicle')}>
                {
                    !vnode.state.vehicles ? (
                        <div>
                            <div class={`form-group ${vnode.state.error && !manufacture.isValid() ? 'has-error' : ''}`}>
                                <label class="form-label">{mx.translate('vehicles.add.manufacture')}:</label>
                                <select class="form-select" onchange={(e) => {
                                    if (e.currentTarget[e.currentTarget.selectedIndex].value === 'Tesla') {
                                        document.getElementById('token').classList.remove('d-none');
                                    } else {
                                        document.getElementById('token').classList.add('d-none');
                                    }
                                    manufacture.setAndValidate(e.currentTarget[e.currentTarget.selectedIndex].value);
                                }}>
                                    <option selected={manufacture() === '' && 'selected'} hidden></option>
                                    <option selected={manufacture() === 'Tesla' && 'selected'} value="Tesla">Tesla</option>
                                    <option selected={manufacture() === 'BMW' && 'selected'} value="BMW">BMW</option>
                                </select>
                                <span class={`form-input-hint ${!vnode.state.error && !manufacture.isValid() ? 'd-none' : ''}`}>
                                    {manufacture.error()}
                                </span>
                            </div>
                            <div class={`form-group ${vnode.state.error && !email.isValid() ? 'has-error' : ''}`}>
                                <label class="form-label">E-Mail:</label>
                                <input class="form-input" type="email"
                                    value={email()}
                                    oninput={e => vnode.state.error ? email.setAndValidate(e.target.value) : email(e.target.value)}
                                />
                                <span class={`form-input-hint ${!vnode.state.error && !email.isValid() ? 'd-none' : ''}`}>
                                    {email.error()}
                                </span>
                            </div>
                            <div class={`form-group ${vnode.state.error && !password.isValid() ? 'has-error' : ''}`}>
                                <label class="form-label">{mx.translate('password')}:</label>
                                <input class="form-input" type="password"
                                    value={password()}
                                    oninput={e => vnode.state.error ? password.setAndValidate(e.target.value) : password(e.target.value)}
                                />
                                <span class={`form-input-hint ${!vnode.state.error && !password.isValid() ? 'd-none' : ''}`}>
                                    {password.error()}
                                </span>
                            </div>
                            <div id="token" class={`form-group d-none ${vnode.state.error && !token.isValid() ? 'has-error' : ''}`}>
                                <br />
                                <div class="divider text-center" data-content="OR"></div>
                                <label class="form-label">Token:</label>
                                <input class="form-input" type="text"
                                    value={token()}
                                    oninput={e => vnode.state.error ? token.setAndValidate(e.target.value) : token(e.target.value)}
                                />
                                <span class={`form-input-hint ${!vnode.state.error && !token.isValid() ? 'd-none' : ''}`}>
                                    {token.error()}
                                </span>
                            </div>
                            <button class={`btn btn-primary mt-2 ${vnode.state.loading && 'loading'}`}
                                onclick={() => { vnode.state.login(); }}>{mx.translate('login.login')}</button>
                        </div>
                    ) : (
                        <form onsubmit={(e) => {
                            e.preventDefault();
                            this.add(e.target[0][e.target[0].selectedIndex].value);
                        }}>
                            <div class="form-group">
                                <label class="form-label">Name:</label>
                                <select class="form-select">
                                    {
                                        vnode.state.vehicles.map((vehicle, index) => {
                                            return <option key={index} value={vehicle.vehicleId}>{vehicle.displayName}</option>;
                                        })
                                    }
                                </select>
                            </div>
                            <button class={`btn btn-primary mt-2 ${vnode.state.loading && 'loading'}`}>{mx.translate('vehicles.add.add')}</button>
                        </form>
                    )
                }
            </ModalLayout>
        );
    },
};