import powerform from 'powerform';
import { isString } from 'validatex';
import ModalLayout from '../../../components/modal-layout';

export default {
    loading: false,
    oninit(vnode) {
        this.error = false;
        this.store = vnode.attrs.store;
        this.reload = vnode.attrs.reload;
        this.form = powerform({
            email: {
                default: '',
                validator: [
                    isString(),
                ],
            },
            password: {
                default: '',
                validator: isString(),
            },
            token: {
                default: '',
                validator: isString(),
            },
        });
    },
    validate(vehicle) {
        this.loading = true;
        this.store.vehicles.update(vehicle._id, {
            deployment: vehicle.deployment,
            manufacture: vehicle.manufacture,
            email: this.form.email(),
            password: this.form.password(),
            token: this.form.token(),
            vehicleId: vehicle.vehicleId,
            displayName: vehicle.displayName,
            model: vehicle.model,
            vin: vehicle.vin,
        }).then(() => {
            this.form.reset();
            document.getElementById('edit').classList.remove('active');
            this.reload();
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.add.error.login'));
            this.loading = false;
        });
    },
    view(vnode) {
        let { email, password, token } = vnode.state.form;
        return (
            <ModalLayout id="edit" title={mx.translate('vehicles.edit.edit')}>
                {
                    !vnode.state.vehicles ? (
                        <div>
                            <div class={`form-group ${vnode.state.error && !email.isValid() ? 'has-error' : ''}`}>
                                <label class="form-label">E-Mail:</label>
                                <input class="form-input" type="email"
                                    value={email()}
                                    oninput={e => vnode.state.error ? email.setAndValidate(e.target.value) : email(e.target.value)}
                                />
                                <span class={`form-input-hint ${!vnode.state.error && !email.isValid() ? 'd-none' : ''}`}>
                                    {email.error()}
                                </span>
                            </div>
                            <div class={`form-group ${vnode.state.error && !password.isValid() ? 'has-error' : ''}`}>
                                <label class="form-label">{mx.translate('password')}:</label>
                                <input class="form-input" type="password"
                                    value={password()}
                                    oninput={e => vnode.state.error ? password.setAndValidate(e.target.value) : password(e.target.value)}
                                />
                                <span class={`form-input-hint ${!vnode.state.error && !password.isValid() ? 'd-none' : ''}`}>
                                    {password.error()}
                                </span>
                            </div>
                            {
                                vnode.attrs.vehicle.manufacture === 'Tesla' &&
                                <div class={`form-group ${vnode.state.error && !token.isValid() ? 'has-error' : ''}`}>
                                    <br />
                                    <div class="divider text-center" data-content="OR"></div>
                                    <label class="form-label">Token:</label>
                                    <input class="form-input" type="text"
                                        value={token()}
                                        oninput={e => vnode.state.error ? token.setAndValidate(e.target.value) : token(e.target.value)}
                                    />
                                    <span class={`form-input-hint ${!vnode.state.error && !token.isValid() ? 'd-none' : ''}`}>
                                        {token.error()}
                                    </span>
                                </div>
                            }
                            <button class={`btn btn-primary mt-2 ${vnode.state.loading && 'loading'}`}
                                onclick={() => { vnode.state.validate(vnode.attrs.vehicle); }}>{mx.translate('vehicles.edit.validate')}</button>
                        </div>
                    ) : (
                        <form onsubmit={(e) => {
                            e.preventDefault();
                            this.add(e.target[0].value);
                        }}>
                            <div class="form-group">
                                <label class="form-label">{mx.translate('vehicles.add.manufacture')}:</label>
                                <select class="form-select">
                                    {
                                        vnode.state.vehicles.map((vehicle, index) => {
                                            return <option key={index} value={vehicle.vehicleId}>{vehicle.displayName}</option>;
                                        })
                                    }
                                </select>
                            </div>
                            <button class={`btn btn-primary mt-2 ${vnode.state.loading && 'loading'}`}>{mx.translate('vehicles.add.add')}</button>
                        </form>
                    )
                }
            </ModalLayout>
        );
    },
};