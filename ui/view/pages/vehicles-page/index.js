import './styles.scss';
import moment from 'moment';
import AddDialog from './dialogs/add-dialog';
import EditDialog from './dialogs/edit-dialog';
import DeleteDialog from './dialogs/delete-dialog';

export default {
    loading: false,
    vehicle: {},
    vehicles: [],
    oninit(vnode) {
        this.store = vnode.attrs.store;
        this.reload();
    },
    add() {
        document.getElementById('add').classList.add('active');
    },
    edit(vehicle) {
        this.vehicle = vehicle;
        document.getElementById('edit').classList.add('active');
    },
    del(id) {
        this.id = id;
        document.getElementById('del').classList.add('active');
    },
    patch(id, sync) {
        this.store.vehicles.patch(id, { sync }).then(() => {
            this.reload();
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.error.patch'));
        });
    },
    reload() {
        this.loading = true;
        this.store.vehicles.list().then(vehicles => {
            this.vehicles = vehicles;
            this.loading = false;
            m.redraw();
        }).catch(() => {
            this.store.toast.showMessage('error', mx.translate('vehicles.error.load'));
            this.loading = false;
            m.redraw();
        });
    },
    view(vnode) {
        return (
            <main class="container grid-lg vehicles" >
                {
                    vnode.state.loading &&
                    <div class="text-center">
                        <div class="loading loading-lg mb-2"></div>
                        {mx.translate('loading')}
                    </div>
                }
                {
                    (!vnode.state.loading && vnode.state.vehicles.length === 0) &&
                    <div class="empty">
                        <div class="empty-icon">
                            <i class="fas fa-car fa-5x"></i>
                        </div>
                        <p class="empty-title h5">{mx.translate('vehicles.you')}</p>
                        <p class="empty-subtitle">{mx.translate('vehicles.click')}</p>
                        <div class="empty-action">
                            <button class="btn btn-primary" onclick={vnode.state.add}>{mx.translate('vehicles.add-vehicle')}</button>
                        </div>
                    </div>
                }
                {
                    (!vnode.state.loading && vnode.state.vehicles.length > 0) &&
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-gray h5 ">
                                <div class="columns">
                                    <div class="column col-xs-12 col-6">
                                        {mx.translate('vehicle')}
                                    </div>
                                    <div class="column col-xs-12 col-6 text-right">
                                        <div class="dropdown dropdown-right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="divider"></div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>{mx.translate('vehicles.brand')}</th>
                                        <th>{mx.translate('vehicles.model')}</th>
                                        <th>VIN</th>
                                        <th>Name</th>
                                        <th>{mx.translate('vehicles.last')}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        vnode.state.vehicles.map((vehicle) =>
                                            <tr key={vehicle._id}>
                                                <td>{vehicle.manufacture}</td>
                                                <td>{vehicle.model}</td>
                                                <td>{vehicle.vin}</td>
                                                <td>{vehicle.displayName}</td>
                                                <td>
                                                    {vehicle.lastSync && moment(vehicle.lastSync).format('lll')}<br />
                                                    {
                                                        vehicle.ip &&
                                                        <span>IP: {vehicle.ip}</span>
                                                    }
                                                </td>
                                                <td class="text-right">
                                                    {
                                                        vehicle.manufacture === 'Tesla' &&
                                                        <button class="btn btn-primary btn-sm tooltip"
                                                            data-tooltip={mx.translate('vehicles.sleepInterval')}
                                                            onclick={() => {
                                                                m.route.set(`/vehicles/${vehicle._id}/sleep`);
                                                            }}>
                                                            <i class="icon icon-time"></i>
                                                        </button>
                                                    }
                                                    <button class="btn btn-primary btn-sm tooltip ml-1"
                                                        data-tooltip={mx.translate('vehicles.update')}
                                                        onclick={() => { vnode.state.edit(vehicle); }}>
                                                        <i class="icon icon-edit"></i>
                                                    </button>
                                                    {
                                                        vehicle.sync ? (
                                                            <button class="btn btn-error btn-sm ml-1 tooltip"
                                                                data-tooltip={mx.translate('vehicles.stop')}
                                                                onclick={() => { vnode.state.patch(vehicle._id, false); }}>
                                                                <i class="icon icon-stop"></i>
                                                            </button>
                                                        ) : (
                                                            <button class="btn btn-success btn-sm ml-1 tooltip"
                                                                data-tooltip={mx.translate('vehicles.start')}
                                                                onclick={() => { vnode.state.patch(vehicle._id, true); }}>
                                                                <i class="icon icon-arrow-right"></i>
                                                            </button>
                                                        )
                                                    }
                                                    <button class="btn btn-error btn-sm ml-1 tooltip"
                                                        data-tooltip={mx.translate('vehicles.del')}
                                                        onclick={() => { vnode.state.del(vehicle._id); }}>
                                                        <i class="icon icon-delete"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    }
                                </tbody>
                            </table>
                            {
                                vnode.state.vehicles[0].valid === false &&
                                <p class="text-right mt-2">
                                    <br />
                                    <span class="text-error text-bold">{mx.translate('vehicles.warning')}:</span> {mx.translate('vehicles.login')}
                                </p>
                            }
                        </div>
                    </div>
                }
                <AddDialog store={vnode.attrs.store}
                    reload={() => { vnode.state.reload(); }} />
                <EditDialog store={vnode.attrs.store} vehicle={vnode.state.vehicle}
                    reload={() => { vnode.state.reload(); }} />
                <DeleteDialog store={vnode.attrs.store} id={vnode.state.id}
                    reload={() => { vnode.state.reload(); }} />
            </main >
        );
    },
};