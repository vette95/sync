export default (url, users) => {
    return {
        list() {
            return new Promise(async (resolve, reject) => {
                try {
                    const vehicles = await m.request({
                        method: 'GET',
                        url: `${url}/v1/vehicles?userId=${users.data.id}`,
                        headers: { 'Authorization': `Bearer ${users.getToken()}` },
                    });
                    const vehicleSync = [];
                    for (const vehicle of vehicles) {
                        switch (vehicle.manufacture) {
                        case 'BMW':
                            vehicleSync.push(m.request({
                                method: 'GET',
                                url: `${url}/v1/bmw-data?vehicleId=${vehicle._id}&$limit=1&$sort[createdAt]=-1&$select[]=createdAt&$select[]=ip`,
                                headers: { 'Authorization': `Bearer ${users.getToken()}` },
                            }));
                            break;
                        default:
                            vehicleSync.push(m.request({
                                method: 'GET',
                                url: `${url}/v1/tesla-data?vehiclesId=${vehicle._id}&$limit=1&$sort[createdAt]=-1&$select[]=createdAt&$select[]=ip`,
                                headers: { 'Authorization': `Bearer ${users.getToken()}` },
                            }));
                        }
                    }
                    const syncData = await Promise.all(vehicleSync);
                    const vehiclesCheck = await m.request({
                        method: 'GET',
                        url: `http://${location.hostname}:8080/vehicles/check`,
                        headers: { 'Authorization': `Bearer ${users.getToken()}` },
                    });
                    for (const key in vehicles) {
                        if (syncData[key].length === 1) {
                            vehicles[key].lastSync = syncData[key][0].createdAt;
                            vehicles[key].ip = syncData[key][0].ip;
                        }
                        vehicles[key].valid = false;
                        for (const vehicle of vehiclesCheck) {
                            if (vehicle.id === vehicles[key]._id) {
                                if (vehicle.valid) {
                                    vehicles[key].valid = true;
                                }
                                vehicles[key].sync = vehicle.sync;
                                break;
                            }
                        }
                    }
                    resolve(vehicles);
                } catch (e) {
                    reject();
                }
            });
        },
        get(id) {
            return new Promise(async (resolve, reject) => {
                try {
                    const vehicle = await m.request({
                        method: 'GET',
                        url: `${url}/v1/vehicles/${id}`,
                        headers: { 'Authorization': `Bearer ${users.getToken()}` },
                    });
                    const vehiclesCheck = await m.request({
                        method: 'GET',
                        url: `http://${location.hostname}:8080/vehicles/check`,
                        headers: { 'Authorization': `Bearer ${users.getToken()}` },
                    });
                    for (const obj of vehiclesCheck) {
                        if (obj.id === vehicle._id) {
                            vehicle.flex = obj.flex;
                            vehicle.predict = obj.predict;
                            vehicle.manual = obj.manual;
                            vehicle.timeStartSleep = obj.timeStartSleep;
                            vehicle.timeWaitSleep = obj.timeWaitSleep;
                            vehicle.fix = obj.fix;
                            vehicle.time = obj.time;
                            break;
                        }
                    }
                    resolve(vehicle);
                } catch (e) {
                    reject();
                }
            });
        },
        validate(data) {
            return m.request({
                method: 'POST',
                url: `http://${location.hostname}:8080/validate`,
                data,
            });
        },
        add(data) {
            return m.request({
                method: 'POST',
                url: `http://${location.hostname}:8080/vehicles`,
                headers: { 'Authorization': `Bearer ${users.getToken()}` },
                data,
            });
        },
        del(id) {
            return m.request({
                method: 'DELETE',
                url: `http://${location.hostname}:8080/vehicles/${id}`,
                headers: { 'Authorization': `Bearer ${users.getToken()}` },
            });
        },
        update(id, data) {
            return m.request({
                method: 'PUT',
                url: `http://${location.hostname}:8080/vehicles/${id}`,
                headers: { 'Authorization': `Bearer ${users.getToken()}` },
                data,
            });
        },
        patch(id, data) {
            return m.request({
                method: 'PATCH',
                url: `http://${location.hostname}:8080/vehicles/${id}`,
                headers: { 'Authorization': `Bearer ${users.getToken()}` },
                data,
            });
        },
    };
};