import lang from './lang';
import users from './users';
import vehicles from './vehicles';

const url = 'https://api.tronity.io';
const usersStore = users(url);
export default {
    users: usersStore,
    vehicles: vehicles(url, usersStore),
    lang: lang(),
    toast: {
        show: false,
        type: 'primary',
        message: '',
        showMessage(type, message) {
            this.type = type || this.type;
            this.message = message;
            this.show = true;
            m.redraw();
            setTimeout(() => {
                this.show = false;
                m.redraw();
            }, 5000);
        },
        close() {
            this.show = false;
        },
    },
};