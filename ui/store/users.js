export default (url) => {
    return {
        data: {},
        init() {
            if (localStorage.getItem('token')) {
                if (localStorage.getItem('expired') > (new Date()).getTime()) {
                    this.data = JSON.parse(localStorage.getItem('user'));
                } else {
                    this.logout();
                }
            }
        },
        getToken() {
            if (localStorage.getItem('token')) {
                if (localStorage.getItem('expired') < (new Date()).getTime()) {
                    return this.logout();
                }

                return localStorage.getItem('token');
            } else {
                return this.logout();
            }
        },
        login(data) {
            return new Promise((resolve, reject) => {
                m.request({
                    method: 'POST',
                    url: `${url}/authentication`,
                    extract: (xhr) => {
                        return JSON.parse(xhr.responseText);
                    },
                    data,
                }).then(dataToken => {
                    m.request({
                        method: 'POST',
                        url: `${url}/authentication`,
                        extract: (xhr) => {
                            return JSON.parse(xhr.responseText);
                        },
                        data,
                    }).then(data => {
                        this.data = data[0];
                        localStorage.setItem('user', JSON.stringify(this.data));
                        localStorage.setItem('token', dataToken.accessToken);
                        localStorage.setItem('expired', dataToken.expired);
                        resolve();
                    }).catch((e) => {
                        reject(e);
                    });
                }).catch((e) => {
                    reject(e);
                });
            });
        },
        logout() {
            this.data = {};
            localStorage.clear();
            if (m.route.get())
                m.route.set('/login');
        },
    };
};