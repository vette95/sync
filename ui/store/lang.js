export default () => {
    return {
        value: 'en',
        set(value) {
            if (!value) {
                switch (window.navigator.userLanguage || window.navigator.language) {
                case 'de-DE':
                    this.value = 'de';
                    break;
                default:
                    this.value = 'en';
                }
            } else {
                this.value = value;
            }

            mx.translate.configure({ infix: '/i18n/', suffix: '.json' });
            return mx.translate.use(this.value);
        },
    };
};