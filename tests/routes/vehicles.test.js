/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const fs = require('fs');
const sinon = require('sinon');
const jwt = require('jsonwebtoken');
const bmw = require('../../api/connectors/bmw');
const tesla = require('../../api/connectors/tesla');
const tronity = require('../../api/connectors/tronity');
const vehicles = require('../../api/routes/vehicles');

describe('/routes', () => {
    describe('/validate', () => {
        beforeEach(() => {
            sinon.stub(bmw, 'vehicles').returns(new Promise(resolve => {
                resolve('BMW');
            }));
            sinon.stub(tesla, 'vehicles').returns(new Promise(resolve => {
                resolve('Tesla');
            }));
        });
        afterEach(() => {
            sinon.restore();
        });
        it('BMW', (done) => {
            vehicles.validate({
                body: {
                    manufacture: 'BMW',
                },
            }, {
                send(data) {
                    data.should.eql('BMW');
                    done();
                },
            });
        });
        it('Tesla', (done) => {
            vehicles.validate({
                body: {
                    manufacture: 'Tesla',
                },
            }, {
                send(data) {
                    data.should.eql('Tesla');
                    done();
                },
            });
        });
        it('Fail', (done) => {
            bmw.vehicles.restore();
            sinon.stub(bmw, 'vehicles').returns(new Promise((resolve, reject) => {
                reject();
            }));
            vehicles.validate({
                body: {
                    manufacture: 'BMW',
                },
            }, {
                status(code) {
                    return {
                        send(data) {
                            code.should.eql(401);
                            data.should.eql({
                                code: 401,
                                message: 'Login was not possible!',
                            });
                            done();
                        },
                    };
                },
            });
        });
    });

    describe('/vehicles', () => {
        beforeEach(() => {
            this.decode = sinon.stub(jwt, 'decode').returns({ userId: '' });
            sinon.stub(tronity, 'addVehicle').returns(new Promise(resolve => {
                resolve({ _id: 'id', vin: 'vin' });
            }));
            sinon.stub(tronity, 'checkClient').returns(new Promise(resolve => {
                resolve();
            }));
            sinon.stub(tronity, 'createClient').returns(new Promise(resolve => {
                resolve({ clientId: 'clientId', clientSecret: 'clientSecret' });
            }));
            sinon.stub(fs, 'existsSync').returns(false);
            sinon.stub(bmw, 'vehicles').returns(new Promise(resolve => {
                resolve([{ vin: 'vin', vehicleId: 'vehicleId' }]);
            }));
            sinon.stub(tesla, 'vehicles').returns(new Promise(resolve => {
                resolve([{ vin: 'vin', vehicleId: 'vehicleId' }]);
            }));
        });
        afterEach(() => {
            sinon.restore();
        });
        it('Vehicle not exists', (done) => {
            let file;
            sinon.stub(tronity, 'checkVehicle').returns(new Promise(resolve => {
                resolve([]);
            }));
            sinon.stub(tronity, 'patchVehicle').returns(new Promise(resolve => {
                resolve();
            }));
            sinon.replace(fs, 'writeFile', (path, data, cb) => {
                file = JSON.parse(data);
                return cb(null);
            });
            vehicles.create({
                headers: {
                    authorization: '',
                },
                body: {
                    manufacture: 'manufacture',
                    email: 'email',
                    password: 'password',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            code.should.eql(201);
                            file.should.eql([{
                                id: 'id',
                                vin: 'vin',
                                clientId: 'clientId',
                                clientSecret: 'clientSecret',
                                manufacture: 'manufacture',
                                email: 'email',
                                password: 'password',
                                flex: true,
                                predict: true,
                                manual: false,
                                fix: false,
                                wakeUp: false,
                            }]);
                            done();
                        },
                    };
                },
            });
        });
        it('Vehicle exists', (done) => {
            let file;
            sinon.stub(tronity, 'checkVehicle').returns(new Promise(resolve => {
                resolve([{
                    _id: 'id', vin: 'exists',
                }]);
            }));
            sinon.stub(tronity, 'patchVehicle').returns(new Promise(resolve => {
                resolve();
            }));
            sinon.replace(fs, 'writeFile', (path, data, cb) => {
                file = JSON.parse(data);
                return cb(null);
            });
            vehicles.create({
                headers: {
                    authorization: '',
                },
                body: {
                    manufacture: 'manufacture',
                    email: 'email',
                    password: 'password',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            file.should.eql([{
                                id: 'id',
                                vin: 'exists',
                                clientId: 'clientId',
                                clientSecret: 'clientSecret',
                                manufacture: 'manufacture',
                                email: 'email',
                                password: 'password',
                                flex: true,
                                predict: true,
                                manual: false,
                                fix: false,
                                wakeUp: false,
                            }]);
                            code.should.eql(201);
                            done();
                        },
                    };
                },
            });
        });
        it('Vehicle data exists', (done) => {
            let file;
            fs.existsSync.restore();
            sinon.stub(fs, 'existsSync').returns(true);
            sinon.stub(tronity, 'checkVehicle').returns(new Promise(resolve => {
                resolve([{
                    _id: 'id', vin: 'exists',
                }]);
            }));
            sinon.stub(tronity, 'patchVehicle').returns(new Promise(resolve => {
                resolve();
            }));
            sinon.stub(fs, 'readFileSync').returns(JSON.stringify([{
                id: 'id',
                vin: 'exists',
                clientId: 'clientId',
                clientSecret: 'clientSecret',
                manufacture: 'manufacture',
                email: 'email',
                password: 'password',
            }]));
            sinon.replace(fs, 'writeFile', (path, data, cb) => {
                file = JSON.parse(data);
                return cb(null);
            });
            vehicles.create({
                headers: {
                    authorization: '',
                },
                body: {
                    manufacture: 'manufacture',
                    email: 'email',
                    password: 'password',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            file.should.eql([{
                                id: 'id',
                                vin: 'exists',
                                clientId: 'clientId',
                                clientSecret: 'clientSecret',
                                manufacture: 'manufacture',
                                email: 'email',
                                password: 'password',
                                flex: true,
                                predict: true,
                                manual: false,
                                fix: false,
                                wakeUp: false,
                            }]);
                            code.should.eql(201);
                            done();
                        },
                    };
                },
            });
        });
        it('Vehicle check', (done) => {
            fs.existsSync.restore();
            sinon.stub(fs, 'existsSync').returns(true);
            sinon.stub(tronity, 'getVehicle').returns(new Promise(resolve => {
                resolve({ sync: false });
            }));
            sinon.stub(fs, 'readFileSync').returns(JSON.stringify([{
                id: 'id',
                vin: 'exists',
                clientId: 'clientId',
                clientSecret: 'clientSecret',
                manufacture: 'manufacture',
                email: 'email',
                password: 'password',
                time: {},
                timeStartSleep: null,
                timeWaitSleep: null,
                flex: true,
                predict: true,
                manual: false,
                fix: false,
                wakeUp: false,
                wakeUpTime: {},
            }]));
            vehicles.check({
                headers: {
                    authorization: '',
                },
                body: {
                    manufacture: 'manufacture',
                    email: 'email',
                    password: 'password',
                },
            }, {
                send(vehicles) {
                    vehicles.should.eql([{
                        id: 'id',
                        valid: true,
                        sync: false,
                        flex: true,
                        predict: true,
                        manual: false,
                        timeStartSleep: null,
                        timeWaitSleep: null,
                        fix: false,
                        time: {},
                        wakeUp: false,
                        wakeUpTime: {},
                    }]);
                    done();
                },
            });
        });
        it('Vehicle update', (done) => {
            let file;
            fs.existsSync.restore();
            sinon.stub(fs, 'existsSync').returns(true);
            sinon.stub(tronity, 'checkVehicle').returns(new Promise(resolve => {
                resolve([{
                    _id: 'id', vin: 'exists',
                }]);
            }));
            sinon.stub(tronity, 'patchVehicle').returns(new Promise(resolve => {
                resolve();
            }));
            sinon.stub(fs, 'readFileSync').returns(JSON.stringify([{
                id: 'id',
                vin: 'exists',
                clientId: 'clientId',
                clientSecret: 'clientSecret',
                manufacture: 'manufacture',
                email: 'email',
                password: 'password',
                flex: true,
                predict: true,
                manual: false,
                fix: false,
                wakeUp: false,
            }]));
            sinon.replace(fs, 'writeFile', (path, data, cb) => {
                file = JSON.parse(data);
                return cb(null);
            });
            vehicles.update({
                headers: {
                    authorization: '',
                },
                body: {
                    vehicleId: 'vehicleId',
                    manufacture: 'manufacture',
                    email: 'email',
                    password: 'password',
                    vin: 'vin',
                },
                params: {
                    id: 'id',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            file.should.eql([{
                                id: 'id',
                                vin: 'vin',
                                clientId: 'clientId',
                                clientSecret: 'clientSecret',
                                vehicleId: 'vehicleId',
                                manufacture: 'manufacture',
                                email: 'email',
                                password: 'password',
                                flex: true,
                                predict: true,
                                manual: false,
                                fix: false,
                                wakeUp: false,
                            }]);
                            code.should.eql(200);
                            done();
                        },
                    };
                },
            });
        });
        it('Vehicle patch', (done) => {
            let file;
            fs.existsSync.restore();
            sinon.stub(fs, 'existsSync').returns(true);
            sinon.stub(tronity, 'patchVehicle').returns(new Promise(resolve => {
                resolve();
            }));
            sinon.stub(fs, 'readFileSync').returns(JSON.stringify([{
                id: 'id',
                vin: 'exists',
                clientId: 'clientId',
                clientSecret: 'clientSecret',
                manufacture: 'manufacture',
                email: 'email',
                password: 'password',
            }]));
            sinon.replace(fs, 'writeFile', (path, data, cb) => {
                file = JSON.parse(data);
                return cb(null);
            });
            vehicles.patch({
                headers: {
                    authorization: '',
                },
                body: {
                    flex: true,
                    predict: true,
                    manual: true,
                    timeStartSleep: 0,
                    timeWaitSleep: 0,
                    fix: true,
                    time: {},
                },
                params: {
                    id: 'id',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            file.should.eql([{
                                id: 'id',
                                vin: 'exists',
                                clientId: 'clientId',
                                clientSecret: 'clientSecret',
                                manufacture: 'manufacture',
                                email: 'email',
                                password: 'password',
                                flex: true,
                                predict: true,
                                manual: true,
                                timeStartSleep: 0,
                                timeWaitSleep: 0,
                                fix: true,
                                time: {},
                            }]);
                            code.should.eql(200);
                            done();
                        },
                    };
                },
            });
        });
        it('Vehicle delete', (done) => {
            fs.existsSync.restore();
            sinon.stub(fs, 'readFileSync').returns(JSON.stringify([{
                id: 'id',
                vin: 'exists',
                clientId: 'clientId',
                clientSecret: 'clientSecret',
                manufacture: 'manufacture',
                email: 'email',
                password: 'password',
            }]));
            sinon.stub(tronity, 'deleteVehicle').returns(new Promise(resolve => {
                resolve();
            }));
            sinon.replace(fs, 'writeFile', (path, data, cb) => {
                data = JSON.parse(data);
                data.should.eql([]);
                return cb(null);
            });
            vehicles.del({
                headers: {
                    authorization: '',
                },
                params: {
                    id: 'id',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            code.should.eql(204);
                            done();
                        },
                    };
                },
            });
        });
        it('Vehicle delete fail', (done) => {
            sinon.stub(fs, 'readFileSync').returns(JSON.stringify([{
                id: 'id',
                vin: 'exists',
                clientId: 'clientId',
                clientSecret: 'clientSecret',
                manufacture: 'manufacture',
                email: 'email',
                password: 'password',
            }]));
            sinon.stub(tronity, 'deleteVehicle').returns(new Promise((resolve, reject) => {
                reject();
            }));
            vehicles.del({
                headers: {
                    authorization: '',
                },
                params: {
                    id: 'id',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            code.should.eql(500);
                            done();
                        },
                    };
                },
            });
        });
        it('Fail', (done) => {
            fs.existsSync.restore();
            sinon.stub(fs, 'existsSync').returns(true);
            sinon.stub(tronity, 'checkVehicle').returns(new Promise((resolve, reject) => {
                reject();
            }));
            vehicles.create({
                headers: {
                    authorization: '',
                },
                body: {
                    manufacture: 'manufacture',
                    email: 'email',
                    password: 'password',
                },
            }, {
                status(code) {
                    return {
                        send() {
                            code.should.eql(500);
                            done();
                        },
                    };
                },
            });
        });
    });
});
