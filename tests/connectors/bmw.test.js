/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require('chai');
const sinon = require('sinon');
const cache = require('memory-cache');
const request = require('request-promise-native');
const bmw = require('../../api/connectors/bmw');
const should = chai.should();

describe('/connectors', () => {
    describe('/bmw', () => {
        afterEach(() => {
            sinon.restore();
        });
        it('token', (done) => {
            let config = {};
            sinon.replace(request, 'post', (obj) => {
                return new Promise((resolve, reject) => {
                    config = obj;
                    reject({
                        statusCode: 302,
                        response: {
                            headers: {
                                location: `${obj.form.redirect_uri}#state=${obj.form.state}&access_token=token&token_type=Bearer&expires_in=7199`,
                            },
                        },
                    });
                });
            });
            bmw.token({
                email: 'email',
                password: 'password',
            }).then((token) => {
                token.should.eql('token');
                config.should.eql({
                    url: 'https://customer.bmwgroup.com/gcdm/oauth/authenticate',
                    form:
                    {
                        username: 'email',
                        password: 'password',
                        client_id: 'dbf0a542-ebd1-4ff0-a9a7-55172fbfce35',
                        response_type: 'token',
                        redirect_uri:
                            'https://www.bmw-connecteddrive.com/app/default/static/external-dispatch.html',
                        scope: 'authenticate_user fupo',
                        state: config.form.state,
                    },
                });
                done();
            });
        });
        it('token fail', (done) => {
            sinon.replace(cache, 'get', () => {
                return null;
            });
            sinon.replace(request, 'post', (obj) => {
                return new Promise((resolve, reject) => {
                    reject({
                        statusCode: 500,
                        response: {
                            headers: {
                                location: '',
                            },
                        },
                    });
                });
            });
            bmw.token({
                email: 'email',
                password: 'password',
            }).catch(() => {
                done();
            });
        });
        it('vehicles', (done) => {
            let config = {};
            sinon.stub(bmw, 'token').returns('test');
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://www.bmw-connecteddrive.de/api/vehicle/navigation/v1/vin') {
                    return new Promise(resolve => {
                        resolve({ socmax: 0 });
                    });
                } else {
                    config = obj;
                    return new Promise(resolve => {
                        resolve([{ modelName: 'modelName', vin: 'vin' }]);
                    });
                }
            });
            bmw.vehicles().then((vehicles) => {
                config.should.eql({
                    url: 'https://www.bmw-connecteddrive.de/api/me/vehicles/v2',
                    headers: { Authorization: 'Bearer test' },
                    json: true,
                });
                vehicles.should.eql([{
                    vehicleId: 'vin',
                    displayName: 'modelName - vin',
                    model: 'modelName',
                    vin: 'vin',
                    kWh: 0,
                }]);
                done();
            });
        });
        it('vehicles fail', (done) => {
            sinon.stub(bmw, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'get', (obj) => {
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            bmw.vehicles().catch(() => {
                done();
            });
        });
        it('data', (done) => {
            let config = {};
            sinon.stub(bmw, 'token').returns('test');
            sinon.replace(request, 'get', (obj) => {
                config[obj.url] = obj;
                if (obj.url === 'https://www.bmw-connecteddrive.de/api/vehicle/navigation/v1/vehicleId') {
                    return new Promise(resolve => {
                        resolve({ socmax: 0 });
                    });
                } else {
                    return new Promise(resolve => {
                        resolve({ attributesMap: {} });
                    });
                }
            });
            bmw.data({ vehicleId: 'vehicleId' }).then((data) => {
                config.should.eql({
                    'https://www.bmw-connecteddrive.de/api/vehicle/dynamic/v1/vehicleId':
                    {
                        url:
                            'https://www.bmw-connecteddrive.de/api/vehicle/dynamic/v1/vehicleId',
                        headers: { Authorization: 'Bearer test' },
                        json: true,
                    },
                    'https://www.bmw-connecteddrive.de/api/vehicle/efficiency/v1/vehicleId':
                    {
                        url:
                            'https://www.bmw-connecteddrive.de/api/vehicle/efficiency/v1/vehicleId',
                        headers: { Authorization: 'Bearer test' },
                        json: true,
                    },
                    'https://www.bmw-connecteddrive.de/api/vehicle/navigation/v1/vehicleId':
                    {
                        url:
                            'https://www.bmw-connecteddrive.de/api/vehicle/navigation/v1/vehicleId',
                        headers: { Authorization: 'Bearer test' },
                        json: true,
                    },
                });
                data.should.eql({
                    dynamic: { attributesMap: { socMax: 0 } },
                    efficiency: { attributesMap: {} },
                });
                done();
            });
        });
        it('data fail', (done) => {
            let config = {};
            sinon.stub(bmw, 'token').returns('test');
            sinon.replace(request, 'get', (obj) => {
                config[obj.url] = obj;
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            bmw.data({ vehicleId: 'vehicleId' }).catch(() => {
                done();
            });
        });
    });
});
