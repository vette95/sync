/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const sinon = require('sinon');
const request = require('request-promise-native');
const tesla = require('../../api/connectors/tesla');

describe('/connectors', () => {
    describe('/tesla', () => {
        afterEach(() => {
            sinon.restore();
        });
        it('token', (done) => {
            let config = {};
            sinon.replace(request, 'post', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve({ access_token: 'token' });
                });
            });
            tesla.token({
                email: 'email',
                password: 'password',
            }).then((token) => {
                token.access_token.should.eql('token');
                config.should.eql({
                    url: 'https://owner-api.teslamotors.com/oauth/token',
                    headers:
                    {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                    },
                    body:
                    {
                        grant_type: 'password',
                        client_id: '81527cff06843c8634fdc09e8ac0abefb46ac849f38fe1e431c2ef2106796384',
                        client_secret: 'c7257eb71a564034f9419ee651c7d0e5f7aa6bfbd18bafb5c5c033b093bb2fa3',
                        email: 'email',
                        password: 'password',
                    },
                    json: true,
                });
                done();
            });
        });
        it('token fail', (done) => {
            sinon.replace(request, 'post', (obj) => {
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            tesla.token({
                email: 'email',
                password: 'password',
            }).catch(() => {
                done();
            });
        });
        it('vehicles', (done) => {
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://owner-api.teslamotors.com/api/1/vehicles/id_s/data') {
                    return new Promise(resolve => {
                        resolve({
                            response: {
                                vehicle_config: {
                                    car_type: 'modelx',
                                    trim_badging: '75D',
                                },
                                charge_state: {
                                    charge_energy_added: 23.86,
                                    charge_miles_added_rated: 100,
                                    battery_range: 204.8,
                                    battery_level: 65,
                                },
                            },
                        });
                    });
                }
                return new Promise(resolve => {
                    resolve({ response: [{ id_s: 'id_s', display_name: 'display_name', vin: 'vins', option_codes: 'BT37', state: 'offline' }] });
                });
            });
            sinon.replace(request, 'post', () => {
                return new Promise(resolve => {
                    resolve();
                });
            });
            tesla.vehicles({}).then((vehicles) => {
                vehicles.should.eql([{
                    vehicleId: 'id_s',
                    displayName: 'display_name',
                    vin: 'vins',
                    kWh: 75.38461538461539,
                    model: 'X 75D',
                }]);
                done();
            });
        }).timeout(5100);
        it('vehicles fail', (done) => {
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'get', (obj) => {
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            tesla.vehicles().catch(() => {
                done();
            });
        });
        it('vehicle', (done) => {
            let config = {};
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'get', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve({ test: 'test' });
                });
            });
            tesla.vehicle({ vehicleId: 'vehicleId' }).then((data) => {
                config.should.eql({
                    url:
                        'https://owner-api.teslamotors.com/api/1/vehicles/vehicleId',
                    headers:
                    {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent':
                            'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                        authorization: 'Bearer test',
                    },
                    json: true,
                });
                done();
            });
        });
        it('vehicles fail', (done) => {
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'get', (obj) => {
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            tesla.vehicle().catch(() => {
                done();
            });
        });
        it('data', (done) => {
            let config = {};
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'get', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve({ test: 'test' });
                });
            });
            tesla.data({ vehicleId: 'vehicleId' }).then((data) => {
                config.should.eql({
                    url:
                        'https://owner-api.teslamotors.com/api/1/vehicles/vehicleId/vehicle_data',
                    headers:
                    {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent':
                            'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                        authorization: 'Bearer test',
                    },
                    json: true,
                });
                data.should.eql({ test: 'test' });
                done();
            });
        });
        it('data fail', (done) => {
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'get', () => {
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            tesla.data({ vehicleId: 'vehicleId' }).catch(() => {
                done();
            });
        });
        it('wakeUp', (done) => {
            let config = {};
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'post', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve();
                });
            });
            tesla.wakeUp({ vehicleId: 'vehicleId' }).then(() => {
                config.should.eql({
                    url:
                        'https://owner-api.teslamotors.com/api/1/vehicles/vehicleId/wake_up',
                    headers:
                    {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent':
                            'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                        authorization: 'Bearer test',
                    },
                    json: true,
                });
                done();
            });
        });
        it('wakeUp fail', (done) => {
            sinon.stub(tesla, 'token').returns({ access_token: 'test' });
            sinon.replace(request, 'post', () => {
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            tesla.wakeUp({ vehicleId: 'vehicleId' }).catch(() => {
                done();
            });
        });
    });
});
