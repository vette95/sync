/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const sinon = require('sinon');
const moment = require('moment');
const request = require('request-promise-native');
const tronity = require('../../api/connectors/tronity');

describe('/connectors', () => {
    describe('/tronity', () => {
        afterEach(() => {
            sinon.restore();
        });
        it('checkVehicle', (done) => {
            let config = {};
            sinon.replace(request, 'get', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve([]);
                });
            });
            tronity.checkVehicle('token', {
                vin: 'vin',
                userId: 'userId',
            }).then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/vehicles?vin=vin&userId=userId',
                    headers: { Authorization: 'token' },
                    json: true,
                });
                done();
            });
        });
        it('addVehicle', (done) => {
            let config = {};
            sinon.replace(request, 'post', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve([]);
                });
            });
            tronity.addVehicle('token', { test: 'test' }).then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/vehicles',
                    headers: { Authorization: 'token' },
                    body: { test: 'test' },
                    json: true,
                });
                done();
            });
        });
        it('getVehicle', (done) => {
            let config = {};
            sinon.replace(request, 'get', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve([]);
                });
            });
            tronity.getVehicle({ accessToken: 'token' }, 'test').then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/vehicles/test',
                    headers: { Authorization: 'token' },
                    json: true,
                });
                done();
            });
        });
        it('checkClient', (done) => {
            let get = {};
            let del = {};
            sinon.replace(request, 'get', (obj) => {
                get = obj;
                return new Promise(resolve => {
                    resolve({ data: [{ _id: '_id' }] });
                });
            });
            sinon.replace(request, 'del', (obj) => {
                del = obj;
                return new Promise(resolve => {
                    resolve();
                });
            });
            tronity.checkClient('token', { userId: 'userId', vehicleId: 'vehicleId' }).then(() => {
                get.should.eql({
                    url:
                        'https://api.tronity.io/v1/clients?userId=userId&vehicleId=vehicleId',
                    headers: { Authorization: 'token' },
                    json: true,
                });
                del.should.eql({
                    url: 'https://api.tronity.io/v1/clients/_id',
                    headers: { Authorization: 'token' },
                    json: true,
                });
                done();
            });
        });
        it('checkClient fail', (done) => {
            sinon.replace(request, 'get', () => {
                return new Promise((resolve, reject) => {
                    reject();
                });
            });
            tronity.checkClient('token', { userId: 'userId', vehicleId: 'vehicleId' }).catch(() => {
                done();
            });
        });
        it('createClient', (done) => {
            let config = {};
            sinon.replace(request, 'post', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve();
                });
            });
            tronity.createClient('token', { test: 'test' }).then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/clients',
                    headers: { Authorization: 'token' },
                    body: { test: 'test' },
                    json: true,
                });
                done();
            });
        });
        it('getToken', (done) => {
            let config = {};
            sinon.replace(request, 'post', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve();
                });
            });
            tronity.getToken({ clientId: 'clientId', clientSecret: 'clientSecret' }).then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/authentication',
                    body:
                    {
                        clientId: 'clientId',
                        clientSecret: 'clientSecret',
                        strategy: 'client',
                    },
                    json: true,
                });
                done();
            });
        });
        it('sendData - Tesla', (done) => {
            let config = {};
            sinon.replace(request, 'post', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve();
                });
            });
            tronity.sendData({ accessToken: 'test' }, 'Tesla', { test: 'test' }).then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/tesla-data',
                    headers: { Authorization: 'test' },
                    body: { test: 'test' },
                    json: true,
                });
                done();
            });
        });
        it('sendData - BMW', (done) => {
            let config = {};
            sinon.replace(request, 'post', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve();
                });
            });
            tronity.sendData({ accessToken: 'test' }, 'BMW', { test: 'test' }).then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/bmw-data',
                    headers: { Authorization: 'test' },
                    body: { test: 'test' },
                    json: true,
                });
                done();
            });
        });
        it('deleteVehicle', (done) => {
            let config = {};
            sinon.replace(request, 'delete', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve([]);
                });
            });
            tronity.deleteVehicle('token', 'test').then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/vehicles/test',
                    headers: { Authorization: 'token' },
                    json: true,
                });
                done();
            });
        });
        it('deleteCloudSync', (done) => {
            let config = {};
            sinon.replace(request, 'delete', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve([]);
                });
            });
            tronity.deleteCloudSync('token', 'test').then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/cloud-sync?vehicleId=test',
                    headers: { Authorization: 'token' },
                    json: true,
                });
                done();
            });
        });
        it('patchVehicle', (done) => {
            let config = {};
            sinon.replace(request, 'patch', (obj) => {
                config = obj;
                return new Promise(resolve => {
                    resolve([]);
                });
            });
            tronity.patchVehicle('token', 'test', {
                deplyoment: 'Private',
            }).then(() => {
                config.should.eql({
                    url: 'https://api.tronity.io/v1/vehicles/test',
                    headers: { Authorization: 'token' },
                    json: true,
                    body: {
                        deplyoment: 'Private',
                    },
                });
                done();
            });
        });
        it('createSleepIfNotExists', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/tesla-data?$sort[createdAt]=-1&$limit=1&vehiclesId=test&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer') {
                    return new Promise(resolve => {
                        resolve([{
                            vehicle_state: {
                                odometer: 0,
                            },
                            charge_state: {
                                timestamp: 0,
                                battery_level: 0,
                                battery_range: {
                                    $numberDecimal: 0,
                                },
                            },
                        }]);
                    });
                }
            });
            let body;
            sinon.replace(request, 'post', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep') {
                    return new Promise(resolve => {
                        body = obj.body;
                        resolve();
                    });
                }
            });
            tronity.createSleepIfNotExists('test', {
                id: 'test',
            }, { data: [] }).then(() => {
                body.should.eql({
                    vehicleId: 'test',
                    odometer: 0,
                    startTime: 0,
                    startLevel: 0,
                    startRange: 0,
                });
                done();
            });
        });
        it('updateSleep', (done) => {
            const timestamp = (new Date()).getTime();
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/tesla-data?$sort[createdAt]=-1&$limit=1&vehiclesId=test&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer') {
                    return new Promise(resolve => {
                        resolve([{
                            vehicle_state: {
                                odometer: 0,
                            },
                            charge_state: {
                                timestamp,
                                battery_level: 0,
                                battery_range: {
                                    $numberDecimal: 0,
                                },
                            },
                        }]);
                    });
                }
            });
            let body;
            sinon.replace(request, 'patch', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep/test') {
                    return new Promise(resolve => {
                        body = obj.body;
                        resolve();
                    });
                }
            });
            tronity.updateSleep('test', {
                id: 'test',
            }, { data: [{ _id: 'test', startRange: 0 }] }).then(() => {
                body.should.eql({ endTime: timestamp, endLevel: 0, endRange: 0 });
                done();
            });
        });
        it('updateSleep - delete', (done) => {
            const timestamp = (new Date()).getTime();
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/tesla-data?$sort[createdAt]=-1&$limit=1&vehiclesId=test&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer') {
                    return new Promise(resolve => {
                        resolve([{
                            vehicle_state: {
                                odometer: 0,
                            },
                            charge_state: {
                                timestamp,
                                battery_level: 0,
                                battery_range: {
                                    $numberDecimal: 0,
                                },
                            },
                        }]);
                    });
                }
            });
            sinon.replace(request, 'delete', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep/test') {
                    return new Promise(resolve => {
                        resolve();
                    });
                }
            });
            tronity.updateSleep('test', {
                id: 'test',
            }, { data: [{ _id: 'test', startRange: -1 }] }).then(() => {
                done();
            });
        });
        it('checkIfLogging - Manuel Sleeping Time', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [{}] });
                    });
                }
            });
            const day = moment().utc().format('ddd');
            const vehicleObj = {
                id: 'test',
                fix: true,
                time: {},
            };
            vehicleObj.time[day] = {
                start: moment().utc().subtract(1, 'hour').format('HH:mm'),
                end: moment().utc().add(2, 'hour').format('HH:mm'),
            };
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'online' }, vehicleObj).then((log) => {
                log.should.eql({ log: false, weakup: false });
                done();
            });
        });
        it('checkIfLogging - Manuel WakeUp Time', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [{}] });
                    });
                }
            });
            sinon.replace(request, 'post', (obj) => {
                if (obj.url === 'https://owner-api.teslamotors.com/oauth/token') {
                    return new Promise(resolve => {
                        resolve({ access_token: 'test' });
                    });
                } else {
                    return new Promise(resolve => {
                        resolve();
                    });
                }
            });
            const day = moment().utc().format('ddd');
            const vehicleObj = {
                id: 'test',
                wakeUp: true,
                wakeUpTime: {},
            };
            vehicleObj.wakeUpTime[day] = {
                start: moment().utc().subtract(1, 'hour').format('HH:mm'),
                end: moment().utc().add(2, 'hour').format('HH:mm'),
            };
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'online' }, vehicleObj).then((log) => {
                log.should.eql({ log: true, weakup: true });
                done();
            });
        });
        it('checkIfLogging - No Auto Sleep', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [] });
                    });
                }
            });
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'online' }, {
                flex: false,
            }).then((log) => {
                log.should.eql({ log: true, weakup: false });
                done();
            });
        });
        it('checkIfLogging - Intelligent Wake Up', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/vehicles/predict/test') {
                    return new Promise(resolve => {
                        resolve({ weakup: true });
                    });
                }
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [] });
                    });
                }
            });
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'online' }, {
                id: 'test',
                flex: true,
                predict: true,
            }).then((log) => {
                log.should.eql({ log: true, weakup: true });
                done();
            });
        });
        it('checkIfLogging - Car offline / sleep', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [{}] });
                    });
                }
            });
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'offline' }, {
                id: 'test',
                flex: true,
            }).then((log) => {
                log.should.eql({ log: false, weakup: false });
                done();
            });
        });
        it('checkIfLogging - Auto Sleep no values', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [] });
                    });
                }
                if (obj.url === 'https://api.tronity.io/v1/tesla-data?$sort[charge_state.timestamp]=-1&$limit=2&vehiclesId=test&$select[]=drive_state.shift_state&$select[]=charge_state.charging_state&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer&$select[]=vehicle_state.locked&$select[]=climate_state.is_climate_on&$select[]=weakup&$select[]=vehicle_state.sentry_mode&$select[]=vehicle_state.software_update.status') {
                    return new Promise(resolve => {
                        resolve([]);
                    });
                }
            });
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'online' }, {
                id: 'test',
                flex: true,
                timeStartSleep: 2,
            }).then((log) => {
                log.should.eql({ log: true, weakup: false });
                done();
            });
        });
        it('checkIfLogging - Auto Sleep negativ values', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [] });
                    });
                }
                if (obj.url === 'https://api.tronity.io/v1/tesla-data?$sort[charge_state.timestamp]=-1&$limit=2&vehiclesId=test&$select[]=drive_state.shift_state&$select[]=charge_state.charging_state&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer&$select[]=vehicle_state.locked&$select[]=climate_state.is_climate_on&$select[]=weakup&$select[]=vehicle_state.sentry_mode&$select[]=vehicle_state.software_update.status') {
                    return new Promise(resolve => {
                        resolve([{ drive_state: { shift_state: 'D' } }, { drive_state: { shift_state: 'D' } }]);
                    });
                }
            });
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'online' }, {
                id: 'test',
                flex: true,
                timeStartSleep: 2,
            }).then((log) => {
                log.should.eql({ log: true, weakup: false });
                done();
            });
        });
        it('checkIfLogging - Auto Sleep no negativ values', (done) => {
            sinon.replace(request, 'get', (obj) => {
                if (obj.url === 'https://api.tronity.io/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=test&endLevel=null') {
                    return new Promise(resolve => {
                        resolve({ data: [] });
                    });
                }
                if (obj.url === 'https://api.tronity.io/v1/tesla-data?$sort[charge_state.timestamp]=-1&$limit=2&vehiclesId=test&$select[]=drive_state.shift_state&$select[]=charge_state.charging_state&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer&$select[]=vehicle_state.locked&$select[]=climate_state.is_climate_on&$select[]=weakup&$select[]=vehicle_state.sentry_mode&$select[]=vehicle_state.software_update.status') {
                    return new Promise(resolve => {
                        resolve([{
                            weakup: false,
                            drive_state: { shift_state: 'P' },
                            charge_state: {
                                timestamp: (new Date()).getTime(),
                                charging_state: 'Complete',
                            },
                            climate_state: {
                                is_climate_on: false,
                            },
                            vehicle_state: {
                                locked: true,
                                sentry_mode: false,
                                software_update: {
                                    status: '',
                                },
                            },
                        }, {
                            weakup: false,
                            drive_state: { shift_state: 'P' },
                            charge_state: {
                                timestamp: (new Date()).getTime(),
                                charging_state: 'Complete',
                            },
                            climate_state: {
                                is_climate_on: false,
                            },
                            vehicle_state: {
                                locked: true,
                                sentry_mode: false,
                                software_update: {
                                    status: '',
                                },
                            },
                        }]);
                    });
                }
            });
            tronity.checkIfLogging({ accessToken: 'test' }, { state: 'online' }, {
                id: 'test',
                flex: true,
                timeStartSleep: 2,
            }).then((log) => {
                log.should.eql({ log: false, weakup: false });
                done();
            });
        });
    });
});
