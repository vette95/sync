const { resolve } = require('path');
const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: resolve(__dirname, 'ui'),
    entry: {
        main: './index.js',
    },
    output: {
        path: resolve(__dirname, 'dist'),
        filename: '[name]-[hash]-bundle.js',
        chunkFilename: '[name]-[hash]-chunk.js',
    },
    module: {
        rules: [{
            test: /\.html$/,
            loader: 'html-loader',
            options: {
                interpolate: true,
            },
        }, {
            test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                publicPath: '../fonts/',
                outputPath: 'fonts/',
            },
        }, {
            test: /\.js$/,
            include: resolve(__dirname, 'ui/'),
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [[
                        '@babel/preset-env',
                        {
                            'modules': false,
                            'useBuiltIns': 'usage',
                            'targets': {
                                'browsers': [
                                    'last 1 version',
                                    '> 1%',
                                    'IE 10',
                                ],
                            },
                        },
                    ]],
                    plugins: [
                        ['transform-react-jsx', {
                            'pragma': 'm',
                        }],
                        ['@babel/plugin-transform-async-to-generator'],
                    ],
                },
            }],
        }],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html',
        }),
        new webpack.ProvidePlugin({
            m: 'mithril',
        }),
        new CopyPlugin([
            { from: 'i18n', to: 'i18n' },
        ]),
        new webpack.optimize.ModuleConcatenationPlugin(),
    ],
    optimization: {
        runtimeChunk: true,
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
    },
};