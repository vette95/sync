# Tronity Sync
[![License][license-image]][license-url]
[![CodeCov][cov-image]][cov-url]


Tronity – Cloud platform to track and analyze your vehicle. Get insights and learn more about your vehicle and your driving behavior by viewing energy consumption, trips, and other vehicle data such as the current firmware.

## Installation
```sh
docker run -d -p 8080:8080 --name tronity-sync tronity/sync
```

## Development setup
```sh
npm install
npm test
```

## Contributing

1. Fork it (<https://gitlab.com/tronity/sync/-/forks/new>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[license-image]: https://img.shields.io/badge/license-MIT-yellow.svg
[license-url]: https://img.shields.io/badge/license-MIT-yellow.svg
[cov-image]: https://codecov.io/gl/tronity/sync/branch/master/graph/badge.svg
[cov-url]: https://codecov.io/gl/tronity/sync