const fs = require('fs');
const jwt = require('jsonwebtoken');
const bmw = require('../connectors/bmw');
const tesla = require('../connectors/tesla');
const tronity = require('../connectors/tronity');

module.exports = {
    async validate(req, res) {
        let vehicles;
        try {
            switch (req.body.manufacture) {
                case 'BMW':
                    vehicles = await bmw.vehicles(req.body);
                    break;
                default:
                    vehicles = await tesla.vehicles(req.body);
            }
        } catch (e) {
            if (e && e.statusCode && e.statusCode === 408) {
                return res.status(408).send({
                    code: 408,
                    message: 'Vehicle unavailable!',
                });
            }

            return res.status(401).send({
                code: 401,
                message: 'Login was not possible!',
            });
        }
        res.send(vehicles);
    },
    async create(req, res) {
        const token = req.headers.authorization;
        const payload = jwt.decode(token.replace('Bearer ', ''));
        try {
            let vehicle = await tronity.checkVehicle(token, {
                vin: req.body.vin,
                userId: payload.sub,
            });
            if (vehicle.length === 0) {
                vehicle = await tronity.addVehicle(token, {
                    userId: payload.sub,
                    displayName: req.body.displayName,
                    manufacture: req.body.manufacture,
                    deployment: 'Private',
                    model: req.body.model,
                    vin: req.body.vin,
                    sync: true,
                    flex: true,
                    predict: true,
                    manual: false,
                    fix: false,
                    wakeUp: false,
                });
            } else {
                vehicle = vehicle[0];
            }

            await tronity.checkClient(token, {
                userId: payload.sub,
                vehicleId: vehicle._id,
            });
            const client = await tronity.createClient(token, {
                userId: payload.sub,
                vehicleId: vehicle._id,
            });
            let data = [];
            if (fs.existsSync('./api/vehicles.json')) {
                data = JSON.parse(fs.readFileSync('./api/vehicles.json'));
                for (const key in data) {
                    if (data[key].vin === vehicle.vin) {
                        data.splice(key, 1);
                        break;
                    }
                }
            }
            data.push({
                id: vehicle._id,
                vin: vehicle.vin,
                clientId: client.clientId,
                clientSecret: client.clientSecret,
                vehicleId: req.body.vehicleId,
                manufacture: req.body.manufacture,
                email: req.body.email,
                password: req.body.password,
                token: req.body.token,
                flex: true,
                predict: true,
                manual: false,
                fix: false,
                wakeUp: false,
            });
            fs.writeFile('./api/vehicles.json', JSON.stringify(data), () => {
                res.status(201).send();
            });
        } catch (e) {
            res.status(500).send();
        }
    },
    async del(req, res) {
        const token = req.headers.authorization;
        try {
            await tronity.deleteVehicle(token, req.params.id);
            let data = JSON.parse(fs.readFileSync('./api/vehicles.json'));
            for (const key in data) {
                if (data[key].id === req.params.id) {
                    data.splice(key, 1);
                    break;
                }
            }
            fs.writeFile('./api/vehicles.json', JSON.stringify(data), () => {
                res.status(204).send();
            });
        } catch (e) {
            res.status(500).send();
        }
    },
    async check(req, res) {
        const token = req.headers.authorization;
        try {
            const vehicles = [];
            let data = [];
            if (fs.existsSync('./api/vehicles.json')) {
                data = JSON.parse(fs.readFileSync('./api/vehicles.json'));
            }
            for (const vehicle of data) {
                switch (vehicle.manufacture) {
                    case 'BMW':
                        await bmw.vehicles(vehicle);
                        break;
                    default:
                        await tesla.vehicles(vehicle);
                }
                const vehicleBase = await tronity.getVehicle({
                    accessToken: token,
                }, vehicle.id);
                vehicles.push({
                    id: vehicle.id,
                    valid: true,
                    sync: vehicleBase.sync,
                    flex: vehicle.flex,
                    predict: vehicle.predict,
                    manual: vehicle.manual,
                    timeStartSleep: vehicle.timeStartSleep,
                    timeWaitSleep: vehicle.timeWaitSleep,
                    fix: vehicle.fix,
                    time: vehicle.time,
                    wakeUp: vehicle.wakeUp,
                    wakeUpTime: vehicle.wakeUpTime,
                });
            }
            res.send(vehicles);
        } catch (e) {
            res.status(500).send();
        }
    },
    async update(req, res) {
        const token = req.headers.authorization;
        const payload = jwt.decode(token.replace('Bearer ', ''));
        try {
            let data = [];
            if (fs.existsSync('./api/vehicles.json')) {
                data = JSON.parse(fs.readFileSync('./api/vehicles.json'));
            }
            for (const key in data) {
                if (data[key].id === req.params.id) {
                    data.splice(key, 1);
                    break;
                }
            }

            //Check client and then create
            await tronity.checkClient(token, {
                userId: payload.sub,
                vehicleId: req.params.id,
            });
            const client = await tronity.createClient(token, {
                userId: payload.sub,
                vehicleId: req.params.id,
            });
            data.push({
                id: req.params.id,
                vin: req.body.vin,
                clientId: client.clientId,
                clientSecret: client.clientSecret,
                vehicleId: req.body.vehicleId,
                manufacture: req.body.manufacture,
                email: req.body.email,
                password: req.body.password,
                token: req.body.token,
                flex: true,
                predict: true,
                manual: false,
                fix: false,
                wakeUp: false,
            });
            await tronity.patchVehicle(token, req.params.id, {
                sync: true,
            });
            if (req.body.deployment === 'Cloud') {
                await tronity.deleteCloudSync(token, req.params.id);
                await tronity.patchVehicle(token, req.params.id, {
                    deployment: 'Private',
                });
            }

            fs.writeFile('./api/vehicles.json', JSON.stringify(data), () => {
                res.status(200).send();
            });
        } catch (e) {
            return res.status(401).send({
                code: 401,
                message: 'Login was not possible!',
            });
        }
    },
    async patch(req, res) {
        const token = req.headers.authorization;
        try {
            let data = [];
            if (fs.existsSync('./api/vehicles.json')) {
                data = JSON.parse(fs.readFileSync('./api/vehicles.json'));
            }
            for (const key in data) {
                if (data[key].id === req.params.id) {
                    const obj = data[key];
                    if (req.body.hasOwnProperty('sync')) {
                        await tronity.patchVehicle(token, req.params.id, {
                            sync: req.body.sync,
                        });
                    }

                    if (req.body.hasOwnProperty('flex'))
                        obj.flex = req.body.flex;
                    if (req.body.hasOwnProperty('predict'))
                        obj.predict = req.body.predict;
                    if (req.body.hasOwnProperty('manual'))
                        obj.manual = req.body.manual;
                    if (req.body.hasOwnProperty('timeStartSleep'))
                        obj.timeStartSleep = parseInt(req.body.timeStartSleep, 10);
                    if (req.body.hasOwnProperty('timeWaitSleep'))
                        obj.timeWaitSleep = parseInt(req.body.timeWaitSleep, 10);
                    if (req.body.hasOwnProperty('fix'))
                        obj.fix = req.body.fix;
                    if (req.body.hasOwnProperty('time'))
                        obj.time = req.body.time;
                    if (req.body.hasOwnProperty('wakeUp'))
                        obj.wakeUp = req.body.wakeUp;
                    if (req.body.hasOwnProperty('wakeUpTime'))
                        obj.wakeUpTime = req.body.wakeUpTime;

                    data.push(obj);
                    data.splice(key, 1);
                    break;
                }
            }
            fs.writeFile('./api/vehicles.json', JSON.stringify(data), () => {
                res.status(200).send();
            });
        } catch (e) {
            res.status(500).send();
        }
    },
};
