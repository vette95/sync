const api = require('express').Router();
const vehicles = require('./vehicles');

module.exports = () => {
    api.get('/vehicles/check', vehicles.check);
    api.post('/validate', vehicles.validate);
    api.post('/vehicles', vehicles.create);
    api.put('/vehicles/:id', vehicles.update);
    api.patch('/vehicles/:id', vehicles.patch);
    api.delete('/vehicles/:id', vehicles.del);
    return api;
};
