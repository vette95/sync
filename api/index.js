const cors = require('cors');
const express = require('express');
const CronJob = require('cron').CronJob;
const bodyParser = require('body-parser');
const routes = require('./routes');
const cron = require('./cron');

const app = express();
app.use(cors());
app.use(express.static('./dist'));
app.use(bodyParser.json());
app.use(routes());
app.get('/*', function (req, res) {
    res.sendFile('/home/node/app/dist/index.html');
});
app.listen(8080, () => {
    new CronJob('*/1 * * * *', cron, null, true);
    console.log('API is running on port 8080');
});