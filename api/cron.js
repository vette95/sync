/* eslint-disable indent */
/* eslint-disable no-case-declarations */
const fs = require('fs');
const { promisify } = require('util');
const bmw = require('./connectors/bmw');
const tesla = require('./connectors/tesla');
const tronity = require('./connectors/tronity');
const getIP = promisify(require('external-ip')());

const sync = async () => {
    try {
        if (fs.existsSync('./api/vehicles.json')) {
            const data = JSON.parse(fs.readFileSync('./api/vehicles.json'));
            for (const vehicle of data) {
                try {
                    const token = await tronity.getToken(vehicle);
                    const vehicleBase = await tronity.getVehicle(token, vehicle.id);
                    if (vehicleBase.sync) {
                        switch (vehicle.manufacture) {
                            case 'BMW':
                                const bmwData = await bmw.data(vehicle);
                                bmwData.vehicleId = vehicle.id;
                                bmwData.ip = await getIP();
                                await tronity.sendData(token, vehicle.manufacture, bmwData);
                                break;
                            default:
                                const vehicleData = (await tesla.vehicle(vehicle)).response;
                                const check = await tronity.checkIfLogging(token, vehicleData, vehicle);
                                if (check.log) {
                                    const teslaData = (await tesla.data(vehicle)).response;
                                    teslaData.vehiclesId = vehicle.id;
                                    teslaData.ip = await getIP();
                                    teslaData.weakup = check.weakup;
                                    await tronity.sendData(token, vehicle.manufacture, teslaData);
                                }
                        }
                    }
                } catch (e) {
                    try {
                        if (e.statusCode === 404 && vehicle.manufacture === 'Tesla') {
                            const vehicles = await tesla.vehicles(vehicle);
                            const vehicleElm = vehicles.find((elm) => {
                                return elm.vin === vehicle.vin;
                            });
                            if (vehicleElm) {
                                for (const key in data) {
                                    if (data[key].id === vehicle.id) {
                                        const obj = data[key];
                                        obj.vehicleId = vehicleElm.vehicleId;
                                        data.push(obj);
                                        data.splice(key, 1);
                                    }
                                }
                                fs.writeFileSync('./api/vehicles.json', JSON.stringify(data));
                            }
                        }
                    } catch (e) {
                        console.log('--------- Error ---------');
                        console.error(`${new Date().toJSON()} - ${JSON.stringify(e)}`);
                    }
                    console.log('--------- Error ---------');
                    console.error(`${new Date().toJSON()} - Code: ${e.statusCode}, Message: ${e.message}`);
                }
            }
        }
    } catch (e) {
        //
    }
};
module.exports = sync;