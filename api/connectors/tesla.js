/* eslint-disable indent */
const request = require('request-promise-native');

module.exports = {
    token(body) {
        return request.post({
            url: 'https://owner-api.teslamotors.com/oauth/token',
            headers: {
                'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
            },
            body: {
                grant_type: 'password',
                client_id: '81527cff06843c8634fdc09e8ac0abefb46ac849f38fe1e431c2ef2106796384',
                client_secret: 'c7257eb71a564034f9419ee651c7d0e5f7aa6bfbd18bafb5c5c033b093bb2fa3',
                email: body.email,
                password: body.password,
            },
            json: true,
        });
    },
    vehicles(body) {
        return new Promise(async (resolve, reject) => {
            const vehicles = [];
            try {
                let token = body.token;
                if (!token) {
                    token = (await this.token(body)).access_token;
                }
                const data = await request.get({
                    url: 'https://owner-api.teslamotors.com/api/1/vehicles',
                    headers: {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                        authorization: `Bearer ${token}`,
                    },
                    json: true,
                });

                for (const vehicle of data.response) {
                    try {
                        const timeOut = () => {
                            return new Promise(resolve => { setTimeout(() => { resolve(); }, 5000); });
                        };
                        if (vehicle.state === 'offline') {
                            await request.post({
                                url: `https://owner-api.teslamotors.com/api/1/vehicles/${vehicle.id_s}/wake_up`,
                                headers: {
                                    'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                                    'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                                    authorization: `Bearer ${token}`,
                                },
                            });
                            await timeOut();
                        }
                        const vehicleData = await request.get({
                            url: `https://owner-api.teslamotors.com/api/1/vehicles/${vehicle.id_s}/data`,
                            headers: {
                                'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                                'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                                authorization: `Bearer ${token}`,
                            },
                            json: true,
                        });

                        let model;
                        switch (vehicleData.response.vehicle_config.car_type) {
                            case 'modelx':
                                model = 'X';
                                break;
                            case 'model3':
                                model = '3';
                                break;
                            default:
                                model = 'S';
                        }
                        if (vehicleData.response.vehicle_config.trim_badging) {
                            model += ` ${vehicleData.response.vehicle_config.trim_badging}`;
                        }
                        let kWh = vehicleData.response.charge_state.charge_energy_added / vehicleData.response.charge_state.charge_miles_added_rated * vehicleData.response.charge_state.battery_range;
                        kWh = Math.round(kWh * 100 / vehicleData.response.charge_state.battery_level);

                        vehicles.push({
                            vehicleId: vehicle.id_s,
                            displayName: vehicle.display_name,
                            vin: vehicle.vin,
                            kWh,
                            model,
                        });
                    } catch (e) {
                        let kWh;
                        const power = [];
                        power['BT37'] = '75';
                        power['BT40'] = '40';
                        power['BT60'] = '60';
                        power['BT70'] = '70';
                        power['BT85'] = '85';
                        power['BTX4'] = '90';
                        power['BTX5'] = '75';
                        power['BTX6'] = '100';
                        power['BTX7'] = '75';
                        power['BTX8'] = '85';

                        let model = vehicle.vin[3];
                        const codes = vehicle.option_codes.split(',');
                        for (const code of codes) {
                            if (code === 'PBT8' || code === 'PX01') {
                                model += 'P';
                            }
                        }
                        for (const code of codes) {
                            if (power[code]) {
                                kWh = parseInt(power[code]);
                                model += ` ${power[code]}`;
                                break;
                            }
                        }
                        for (const code of codes) {
                            if (code === 'DV4W') {
                                model += 'D';
                            }
                        }

                        vehicles.push({
                            vehicleId: vehicle.id_s,
                            displayName: vehicle.display_name,
                            vin: vehicle.vin,
                            kWh,
                            model,
                        });
                    }
                }
                resolve(vehicles);
            } catch (e) {
                reject(e);
            }
        });
    },
    vehicle(vehicle) {
        return new Promise(async (resolve, reject) => {
            try {
                let token = vehicle.token;
                if (!token) {
                    token = (await this.token(vehicle)).access_token;
                }
                resolve(await request.get({
                    url: `https://owner-api.teslamotors.com/api/1/vehicles/${vehicle.vehicleId}`,
                    headers: {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                        authorization: `Bearer ${token}`,
                    },
                    json: true,
                }));
            } catch (e) {
                reject(e);
            }
        });
    },
    wakeUp(vehicle) {
        return new Promise(async (resolve, reject) => {
            try {
                let token = vehicle.token;
                if (!token) {
                    token = (await this.token(vehicle)).access_token;
                }
                await request.post({
                    url: `https://owner-api.teslamotors.com/api/1/vehicles/${vehicle.vehicleId}/wake_up`,
                    headers: {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                        authorization: `Bearer ${token}`,
                    },
                    json: true,
                });
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    },
    data(vehicle) {
        return new Promise(async (resolve, reject) => {
            try {
                let token = vehicle.token;
                if (!token) {
                    token = (await this.token(vehicle)).access_token;
                }
                resolve(await request.get({
                    url: `https://owner-api.teslamotors.com/api/1/vehicles/${vehicle.vehicleId}/vehicle_data`,
                    headers: {
                        'x-tesla-user-agent': 'TeslaApp/3.4.4-350/fad4a582e/android/8.1.0',
                        'user-agent': 'Mozilla/5.0 (Linux; Android 8.1.0; Pixel XL Build/OPM4.171019.021.D1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36',
                        authorization: `Bearer ${token}`,
                    },
                    json: true,
                }));
            } catch (e) {
                reject(e);
            }
        });
    },
};