const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
const cache = require('memory-cache');
const request = require('request-promise-native');
const reqCallback = require('request');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

module.exports = {
    token(obj) {
        return new Promise(async (resolve, reject) => {
            if (obj.vehicleId && cache.get(obj.vehicleId)) {
                return resolve(JSON.parse(cache.get(obj.vehicleId)));
            }

            const getCodeChallenge = () => {
                let hash = '';
                let result = '';
                while (hash === '' || hash.indexOf('+') !== -1 || hash.indexOf('/') !== -1 || hash.indexOf('=') !== -1 || result.indexOf('+') !== -1 || result.indexOf('/') !== -1) {
                    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    result = '';
                    for (let i = 64; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
                    result = Buffer.from(result).toString('base64');
                    result = result.replace(/=/g, '');
                    hash = crypto.createHash('sha256').update(result).digest('base64');
                    hash = hash.slice(0, hash.length - 1);

                }
                return [result, hash];
            };
            const getNonce = () => {
                const timestamp = Date.now();
                let hash = crypto.createHash('sha256').update(timestamp.toString()).digest('base64');
                hash = hash.slice(0, hash.length - 1);
                return hash;
            };

            const state = uuidv4();
            const jar = reqCallback.jar();
            const nonce = getNonce();
            const clientId = 'vw';
            const xrequest = 'de.volkswagen.carnet.eu.eremote';

            const [code_verifier, codeChallenge] = getCodeChallenge();
            let method = 'GET';
            let form = {};
            let url = 'https://identity.vwgroup.io/oidc/v1/authorize';
            url += '&scope=openid%20profile%20mbb%20email%20cars%20birthdate%20badge%20address%20vin';
            url += '&response_type=id_token%20token%20code';
            url += '&redirect_uri=carnet%3A%2F%2Fidentity-kit%2Flogin';
            url += '&nonce=' + nonce + '&state=' + state;
            url += '&code_challenge=' + codeChallenge + '&code_challenge_method=s256';
            request({
                method: method,
                url: url,
                headers: {
                    'x-requested-with': xrequest,
                },
                jar,
                form: form,
                followAllRedirects: true,
            }, (err, resp, body) => {
                if (err) {
                    return reject(err);
                }

                try {
                    const form = {};
                    try {
                        const dom = new JSDOM(body);
                        for (const formElement of dom.window.document.querySelector('#emailPasswordForm').children) {
                            if (formElement.type === 'hidden') {
                                form[formElement.name] = formElement.value;
                            }
                        }
                        form['email'] = obj.email;
                    } catch (e) {
                        return reject(e);
                    }
                    reqCallback.post({
                        url: 'https://identity.vwgroup.io/signin-service/v1/' + clientId + '/login/identifier',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'x-requested-with': xrequest,
                        },
                        jar,
                        form: form,
                        followAllRedirects: true,
                    }, (err, resp, body) => {
                        if (err) {
                            return reject(err);
                        }

                        try {
                            const dom = new JSDOM(body);
                            const form = {};
                            for (const formElement of dom.window.document.querySelector('#credentialsForm').children) {
                                if (formElement.type === 'hidden') {
                                    form[formElement.name] = formElement.value;
                                }
                            }
                            form['password'] = obj.password;

                            reqCallback.post({
                                url: 'https://identity.vwgroup.io/signin-service/v1/' + clientId + '/login/authenticate',
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                    'x-requested-with': xrequest,
                                },
                                jar,
                                form: form,
                                followAllRedirects: false,
                            }, (err, resp) => {
                                if (err) {
                                    return reject(err);
                                }

                                const userid = resp.headers.location.split('&')[2].split('=')[1];
                                let getRequest = reqCallback.get({
                                    url: resp.headers.location,
                                    headers: {
                                        'x-requested-with': xrequest,
                                    },
                                    jar,
                                    followAllRedirects: true,
                                }, () => {
                                    let hash;
                                    let jwtauth_code;
                                    let jwtid_token;

                                    try {
                                        if (getRequest.uri.hash) {
                                            hash = getRequest.uri.hash;
                                        } else {
                                            hash = getRequest.uri.query;
                                        }
                                        const hashArray = hash.split('&');
                                        hashArray.forEach(hash => {
                                            const harray = hash.split('=');
                                            if (harray[0] === 'code') {
                                                jwtauth_code = harray[1];
                                            }
                                            if (harray[0] === 'id_token') {
                                                jwtid_token = harray[1];
                                            }
                                        });
                                    } catch (e) {
                                        return reject(e);
                                    }

                                    reqCallback.post({
                                        url: 'https://tokenrefreshservice.apps.emea.vwapps.io/exchangeAuthCode',
                                        headers: {
                                            'content-type': 'application/x-www-form-urlencoded',
                                        },
                                        body: 'auth_code=' + jwtauth_code + '&id_token=' + jwtid_token + '&code_verifier=' + code_verifier,
                                        followAllRedirects: false,
                                        json: true,
                                        jar,
                                    }, async (err, resp, body) => {
                                        if (err) {
                                            return reject(err);
                                        }

                                        try {
                                            const data = await request.post({
                                                url: 'https://mbboauth-1d.prd.ece.vwg-connect.com/mbbcoauth/mobile/oauth2/v1/token',
                                                headers: {
                                                    'User-Agent': 'okhttp/3.7.0',
                                                },
                                                form: {
                                                    grant_type: 'id_token',
                                                    token: body.id_token,
                                                    scope: 'sc2:fal',
                                                },
                                                json: true,
                                            });
                                            data.exchange = body;
                                            data.userid = userid;

                                            if (obj.vehicleId) {
                                                cache.put(obj.vehicleId, JSON.stringify(data), 1000 * 60 * 58);
                                            }

                                            resolve(data);
                                        } catch (e) {
                                            reject(e);
                                        }
                                    });
                                });
                            });
                        } catch (e) {
                            reject(e);
                        }
                    });
                } catch (e) {
                    reject(e);
                }
            });
        });
    },
    vehicles(body) {
        return new Promise(async (resolve, reject) => {
            const vehicles = [];
            try {
                const tokenData = await this.token(body);
                const data = await request.get({
                    url: 'https://customer-profile.apps.emea.vwapps.io/v1/customers/' + tokenData.userid + '/realCarData',
                    headers: {
                        'user-agent': 'okhttp/3.7.0',
                        Authorization: `Bearer ${tokenData.exchange.access_token}`,
                    },
                    followAllRedirects: true,
                    json: true,
                });
                for (const vehicle of data.realCars) {
                    vehicles.push({
                        vehicleId: vehicle.vehicleIdentificationNumber,
                        displayName: vehicle.nickname,
                        model: 'e-GOLF',
                        vin: vehicle.vehicleIdentificationNumber,
                        kWh: 24,
                    });
                }
                resolve(vehicles);
            } catch (e) {
                reject(e);
            }
        });
    },
    data(body) {
        return new Promise(async (resolve, reject) => {
            try {
                const country = body.country || 'DE';
                const market = body.market || 'de_DE';
                const token = await this.token(body);

                const data = await Promise.all([
                    request.get({
                        url: `https://msg.volkswagen.de/fs-car/bs/cf/v1/VW/${country}/vehicles/${body.vehicleId}/position`,
                        headers: {
                            Authorization: `Bearer ${token.access_token}`,
                            'X-Market': market,
                        },
                        json: true,
                        timeout: 30000,
                    }),
                    request.get({
                        url: `https://msg.volkswagen.de/fs-car/bs/tripstatistics/v1/VW/${country}/vehicles/${body.vehicleId}/tripdata/shortTerm?type=list`,
                        headers: {
                            Authorization: `Bearer ${token.access_token}`,
                            'X-Market': market,
                        },
                        json: true,
                        timeout: 30000,
                    }),
                    request.get({
                        url: `https://msg.volkswagen.de/fs-car/bs/batterycharge/v1/VW/${country}/vehicles/${body.vehicleId}/charger`,
                        headers: {
                            Authorization: `Bearer ${token.access_token}`,
                            'X-Market': market,
                        },
                        json: true,
                        timeout: 30000,
                    }),
                ]);

                const allTrips = data[1].tripDataList.tripData;
                resolve({
                    data: {
                        position: data[0],
                        trip: allTrips[allTrips.length - 1],
                        charge: {
                            primaryEngineRange: data[2].charger.status.cruisingRangeStatusData.primaryEngineRange.content,
                            chargingMode: data[2].charger.status.chargingStatusData.chargingMode.content,
                            stateOfCharge: data[2].charger.status.batteryStatusData.stateOfCharge.content,
                            plugStatusData: data[2].charger.status.plugStatusData.plugState.content,
                            timestamp: data[2].charger.status.batteryStatusData.stateOfCharge.timestamp,
                        },
                    },
                });
            } catch (e) {
                reject(e);
            }
        });
    },
};