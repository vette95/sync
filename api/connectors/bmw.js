const uuid = require('uuid/v4');
const cache = require('memory-cache');
const request = require('request-promise-native');

module.exports = {
    token(body) {
        return new Promise(async (resolve, reject) => {
            if (cache.get(body.vehicleId)) {
                resolve(cache.get(body.vehicleId));
            }
            const state = uuid();
            try {
                await request.post({
                    url: 'https://customer.bmwgroup.com/gcdm/oauth/authenticate',
                    form: {
                        username: body.email,
                        password: body.password,
                        client_id: 'dbf0a542-ebd1-4ff0-a9a7-55172fbfce35',
                        response_type: 'token',
                        redirect_uri: 'https://www.bmw-connecteddrive.com/app/default/static/external-dispatch.html',
                        scope: 'authenticate_user fupo',
                        state,
                    },
                });
            } catch (e) {
                try {
                    const hash = require('url').parse(e.response.headers['location']).hash;
                    if (e.statusCode !== 302 || !hash) {
                        return reject();
                    }
                    const myURL = hash.split('&');
                    if (myURL[0].substr(myURL[0].indexOf('=') + 1) !== state) {
                        return reject();
                    }
                    const token = myURL[1].substr(myURL[1].indexOf('=') + 1);
                    cache.put(body.vehicleId, token, 1000 * 60 * 58);
                    resolve(token);
                } catch (e) {
                    return reject();
                }
            }
        });
    },
    vehicles(body) {
        return new Promise(async (resolve, reject) => {
            const vehicles = [];
            try {
                const token = await this.token(body);
                const data = await request.get({
                    url: 'https://www.bmw-connecteddrive.de/api/me/vehicles/v2',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                    json: true,
                });
                for (const vehicle of data) {
                    const vehicleData = await request.get({
                        url: `https://www.bmw-connecteddrive.de/api/vehicle/navigation/v1/${vehicle.vin}`,
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                        json: true,
                    });
                    vehicles.push({
                        vehicleId: vehicle.vin,
                        displayName: `${vehicle.modelName} - ${vehicle.vin}`,
                        model: vehicle.modelName,
                        vin: vehicle.vin,
                        kWh: Math.round(vehicleData.socmax),
                    });
                }
                resolve(vehicles);
            } catch (e) {
                reject(e);
            }
        });
    },
    data(vehicle) {
        return new Promise(async (resolve, reject) => {
            try {
                const token = await this.token(vehicle);
                const data = await Promise.all([
                    request.get({
                        url: `https://www.bmw-connecteddrive.de/api/vehicle/dynamic/v1/${vehicle.vehicleId}`,
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                        json: true,
                    }),
                    request.get({
                        url: `https://www.bmw-connecteddrive.de/api/vehicle/efficiency/v1/${vehicle.vehicleId}`,
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                        json: true,
                    }),
                    request.get({
                        url: `https://www.bmw-connecteddrive.de/api/vehicle/navigation/v1/${vehicle.vehicleId}`,
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                        json: true,
                    }),
                ]);
                const dynamic = data[0];
                dynamic.attributesMap.socMax = data[2].socmax;
                console.log(data[2]);
                resolve({
                    dynamic,
                    efficiency: data[1],
                });
            } catch (e) {
                reject(e);
            }
        });
    },
};