const cache = require('memory-cache');
const request = require('request-promise-native');

module.exports = {
    token(body) {
        return new Promise(async (resolve, reject) => {
            if (body.email) {
                try {
                    resolve(await request.post({
                        url: 'https://id.audi.com/v1/token',
                        form: {
                            scope: 'openid profile email mbb offline_access mbbuserid myaudi selfservice:read selfservice:write',
                            response_type: 'token id_token',
                            grant_type: 'password',
                            username: body.email,
                            password: body.password,
                        },
                        headers: {
                            'User-Agent': 'Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.185 Mobile Safari/537.36',
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                            'Accept-Language': 'en-US,en;q=0.9',
                            'Accept-Encoding': 'gzip, deflate',
                            'upgrade-insecure-requests': 1,
                        },
                        json: true,
                        timeout: 30000,
                    }));
                } catch (e) {
                    reject(e);
                }
            } else {
                try {
                    resolve(await request.post({
                        url: 'https://id.audi.com/v1/token',
                        form: {
                            grant_type: 'refresh_token',
                            response_type: 'token id_token',
                            refresh_token: body.refresh_token,
                        },
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded',
                        },
                        json: true,
                        timeout: 30000,
                    }));
                } catch (e) {
                    reject(e);
                }
            }
        });
    },
    dataToken(body) {
        return new Promise(async (resolve, reject) => {
            if (cache.get(body.vehicleId)) {
                return resolve(cache.get(body.vehicleId));
            }

            try {
                const token = await this.token(body);
                const data = await request.post({
                    url: 'https://mbboauth-1d.prd.ece.vwg-connect.com/mbbcoauth/mobile/oauth2/v1/token',
                    headers: {
                    },
                    form: {
                        grant_type: 'id_token',
                        token: token.id_token,
                        scope: 'sc2:fal',
                    },
                    json: true,
                    timeout: 30000,
                });
                cache.put(body.vehicleId, data.access_token, 1000 * 60 * 58);
                resolve(data.access_token);
            } catch (e) {
                reject(e);
            }
        });
    },
    vehicles(body) {
        return new Promise(async (resolve, reject) => {
            const market = body.market || 'de_DE';
            const vehicles = [];
            try {
                const token = await this.token(body);
                const data = await request.get({
                    url: 'https://msg.audi.de/myaudi/vehicle-management/v1/vehicles',
                    headers: {
                        Authorization: `Bearer ${token.access_token}`,
                        'X-Market': market,
                    },
                    json: true,
                    timeout: 30000,
                });
                for (const vehicle of data.vehicles) {
                    vehicles.push({
                        vehicleId: vehicle.vin,
                        displayName: vehicle.title,
                        model: vehicle.model,
                        vin: vehicle.vin,
                        kWh: 83.6,
                    });
                }
                resolve(vehicles);
            } catch (e) {
                reject(e);
            }
        });
    },
    data(body) {
        return new Promise(async (resolve, reject) => {
            try {
                const country = body.country || 'DE';
                const market = body.market || 'de_DE';
                const token = await this.dataToken(body);
                const data = await Promise.all([
                    request.get({
                        url: `https://msg.audi.de/fs-car/bs/cf/v1/Audi/${country}/vehicles/${body.vehicleId}/position`,
                        headers: {
                            Authorization: `Bearer ${token}`,
                            'X-Market': market,
                        },
                        json: true,
                        timeout: 30000,
                    }),
                    request.get({
                        url: `https://msg.audi.de/fs-car/bs/tripstatistics/v1/Audi/${country}/vehicles/${body.vehicleId}/tripdata/shortTerm?type=list`,
                        headers: {
                            Authorization: `Bearer ${token}`,
                            'X-Market': market,
                        },
                        json: true,
                        timeout: 30000,
                    }),
                    request.get({
                        url: `https://msg.audi.de/fs-car/bs/batterycharge/v1/Audi/${country}/vehicles/${body.vehicleId}/charger`,
                        headers: {
                            Authorization: `Bearer ${token}`,
                            'X-Market': market,
                        },
                        json: true,
                        timeout: 30000,
                    }),
                ]);

                const allTrips = data[1].tripDataList.tripData;
                resolve({
                    data: {
                        position: data[0],
                        trip: allTrips[allTrips.length - 1],
                        charge: {
                            primaryEngineRange: data[2].charger.status.cruisingRangeStatusData.primaryEngineRange.content,
                            chargingMode: data[2].charger.status.chargingStatusData.chargingMode.content,
                            actualChargeRate: data[2].charger.status.chargingStatusData.actualChargeRate.content,
                            chargingPower: data[2].charger.status.chargingStatusData.chargingPower.content,
                            stateOfCharge: data[2].charger.status.batteryStatusData.stateOfCharge.content,
                            plugStatusData: data[2].charger.status.plugStatusData.plugState.content,
                            timestamp: data[2].charger.status.batteryStatusData.stateOfCharge.timestamp,
                        },
                    },
                });
            } catch (e) {
                reject(e);
            }
        });
    },
};