const moment = require('moment');
const tesla = require('./tesla');

const url = 'https://api.tronity.io';
const request = require('request-promise-native');
const tronity = {
    checkVehicle(token, data) {
        return request.get({
            url: `${url}/v1/vehicles?vin=${data.vin}&userId=${data.userId}`,
            headers: {
                Authorization: token,
            },
            json: true,
        });
    },
    getVehicle(token, id) {
        return request.get({
            url: `${url}/v1/vehicles/${id}`,
            headers: {
                Authorization: token.accessToken,
            },
            json: true,
        });
    },
    addVehicle(token, data) {
        return request.post({
            url: `${url}/v1/vehicles`,
            headers: {
                Authorization: token,
            },
            body: data,
            json: true,
        });
    },
    patchVehicle(token, id, data) {
        return request.patch({
            url: `${url}/v1/vehicles/${id}`,
            headers: {
                Authorization: token,
            },
            body: data,
            json: true,
        });
    },
    checkClient(token, data) {
        return new Promise(async (resolve, reject) => {
            try {
                const clients = await request.get({
                    url: `${url}/v1/clients?userId=${data.userId}&vehicleId=${data.vehicleId}`,
                    headers: {
                        Authorization: token,
                    },
                    json: true,
                });
                for (const client of clients.data) {
                    await request.del({
                        url: `${url}/v1/clients/${client._id}`,
                        headers: {
                            Authorization: token,
                        },
                        json: true,
                    });
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    },
    createClient(token, data) {
        return request.post({
            url: `${url}/v1/clients`,
            headers: {
                Authorization: token,
            },
            body: data,
            json: true,
        });
    },
    getToken(data) {
        data.strategy = 'client';
        return request.post({
            url: `${url}/authentication`,
            body: {
                strategy: 'client',
                clientId: data.clientId,
                clientSecret: data.clientSecret,
            },
            json: true,
        });
    },
    sendData(token, manufacture, data) {
        let route = `${url}/v1/tesla-data`;
        if (manufacture === 'BMW') {
            route = `${url}/v1/bmw-data`;
        }
        if (manufacture === 'Audi') {
            route = `${url}/v1/audi-data`;
        }
        if (manufacture === 'VW') {
            route = `${url}/v1/vw-data`;
        }

        return request.post({
            url: route,
            headers: {
                Authorization: token.accessToken,
            },
            body: data,
            json: true,
        });
    },
    deleteVehicle(token, id) {
        return request.delete({
            url: `${url}/v1/vehicles/${id}`,
            headers: {
                Authorization: token,
            },
            json: true,
        });
    },
    deleteCloudSync(token, id) {
        return request.delete({
            url: `${url}/v1/cloud-sync?vehicleId=${id}`,
            headers: {
                Authorization: token,
            },
            json: true,
        });
    },
    createSleepIfNotExists(token, vehicleObj, lastSleep) {
        return new Promise(async resolve => {
            if (lastSleep.data.length === 0) {
                const data = await request.get({
                    url: `${url}/v1/tesla-data?$sort[createdAt]=-1&$limit=1&vehiclesId=${vehicleObj.id}&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer`,
                    headers: {
                        Authorization: token.accessToken,
                    },
                    json: true,
                });
                if (data.length > 0) {
                    await request.post({
                        url: `${url}/v1/sleep`,
                        headers: {
                            Authorization: token.accessToken,
                        },
                        json: true,
                        body: {
                            vehicleId: vehicleObj.id,
                            odometer: data[0].vehicle_state.odometer,
                            startTime: data[0].charge_state.timestamp,
                            startLevel: data[0].charge_state.battery_level,
                            startRange: Math.round(data[0].charge_state.battery_range.$numberDecimal),
                        },
                    });
                    console.log(new Date().toJSON() + ' - Create Sleep!');
                }
            }
            resolve();
        });
    },
    updateSleep(token, vehicleObj, lastSleep) {
        return new Promise(async resolve => {
            if (lastSleep.data.length > 0) {
                const data = await request.get({
                    url: `${url}/v1/tesla-data?$sort[createdAt]=-1&$limit=1&vehiclesId=${vehicleObj.id}&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer`,
                    headers: {
                        Authorization: token.accessToken,
                    },
                    json: true,
                });
                if (data.length > 0) {
                    if ((new Date()).getTime() - data[0].charge_state.timestamp < 120000) {
                        const endRange = Math.round(data[0].charge_state.battery_range.$numberDecimal) - lastSleep.data[0].startRange;
                        if (endRange <= 0) {
                            await request.patch({
                                url: `${url}/v1/sleep/${lastSleep.data[0]._id}`,
                                headers: {
                                    Authorization: token.accessToken,
                                },
                                json: true,
                                body: {
                                    endTime: data[0].charge_state.timestamp,
                                    endLevel: data[0].charge_state.battery_level,
                                    endRange: Math.round(data[0].charge_state.battery_range.$numberDecimal),
                                },
                            });
                            console.log(new Date().toJSON() + ' - Update Sleep!');
                        } else {
                            await request.delete({
                                url: `${url}/v1/sleep/${lastSleep.data[0]._id}`,
                                headers: {
                                    Authorization: token.accessToken,
                                },
                                json: true,
                            });
                            console.log(new Date().toJSON() + ' - Delete Sleep!');
                        }
                    }
                }
            }
            resolve();
        });
    },
    checkIfLogging(token, vehicle, vehicleObj) {
        return new Promise(async resolve => {
            try {
                let lastSleep = await request.get({
                    url: `${url}/v1/sleep?$sort[createdAt]=-1&$limit=1&vehicleId=${vehicleObj.id}&endLevel=null`,
                    headers: {
                        Authorization: token.accessToken,
                    },
                    json: true,
                });

                //////////////////////////////////////////////////////
                // Manuel Sleeping Time
                //////////////////////////////////////////////////////
                if (vehicleObj.fix) {
                    const day = moment().utc().format('ddd');
                    if (
                        vehicleObj.time &&
                        vehicleObj.time[day].start &&
                        vehicleObj.time[day].end &&
                        vehicleObj.time[day].start.length === 5 &&
                        vehicleObj.time[day].end.length === 5
                    ) {
                        let start, end;
                        const today = moment().utc().valueOf();
                        const toFloat = (time) => {
                            var hoursMinutes = time.split(/[.:]/);
                            var hours = parseInt(hoursMinutes[0], 10);
                            var minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;
                            return hours + minutes / 60;
                        };

                        start = new Date(`${moment().utc().format('YYYY-MM-DD')}T${vehicleObj.time[day].start}:00`);
                        if (toFloat(vehicleObj.time[day].end) - toFloat(vehicleObj.time[day].start) < 0) {
                            end = new Date(moment(`${moment().utc().format('YYYY-MM-DD')}T${vehicleObj.time[day].end}:00`).add(1, 'day').valueOf());
                        } else {
                            end = new Date(`${moment().utc().format('YYYY-MM-DD')}T${vehicleObj.time[day].end}:00`);
                        }

                        if (today >= start.getTime() && today <= end.getTime()) {
                            console.log(new Date().toJSON() + ' - Manuel Sleeping Time not logging');
                            await tronity.createSleepIfNotExists(token, vehicleObj, lastSleep);
                            return resolve({ log: false, weakup: false });
                        }
                    }
                }

                //////////////////////////////////////////////////////
                // Manuel WakeUp Time
                //////////////////////////////////////////////////////
                if (vehicleObj.wakeUp) {
                    const day = moment().utc().format('ddd');
                    if (
                        vehicleObj.wakeUpTime &&
                        vehicleObj.wakeUpTime[day].start &&
                        vehicleObj.wakeUpTime[day].end &&
                        vehicleObj.wakeUpTime[day].start.length === 5 &&
                        vehicleObj.wakeUpTime[day].end.length === 5
                    ) {
                        let start, end;
                        const today = moment().utc().valueOf();
                        const toFloat = (time) => {
                            var hoursMinutes = time.split(/[.:]/);
                            var hours = parseInt(hoursMinutes[0], 10);
                            var minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;
                            return hours + minutes / 60;
                        };

                        start = new Date(`${moment().utc().format('YYYY-MM-DD')}T${vehicleObj.wakeUpTime[day].start}:00`);
                        if (toFloat(vehicleObj.wakeUpTime[day].end) - toFloat(vehicleObj.wakeUpTime[day].start) < 0) {
                            end = new Date(moment(`${moment().utc().format('YYYY-MM-DD')}T${vehicleObj.wakeUpTime[day].end}:00`).add(1, 'day').valueOf());
                        } else {
                            end = new Date(`${moment().utc().format('YYYY-MM-DD')}T${vehicleObj.wakeUpTime[day].end}:00`);
                        }

                        if (today >= start.getTime() && today <= end.getTime()) {
                            console.log(new Date().toJSON() + ' - Manuel WakeUp Time currenly logging');
                            await tesla.wakeUp(vehicleObj);
                            return resolve({ log: true, weakup: true });
                        }
                    }
                }

                //////////////////////////////////////////////////////
                // No Auto Sleep
                //////////////////////////////////////////////////////
                if (vehicleObj.flex === false) {
                    console.log(new Date().toJSON() + ' - Currently logging');
                    return resolve({ log: true, weakup: false });
                }

                //////////////////////////////////////////////////////
                // Intelligent Wake Up
                //////////////////////////////////////////////////////
                if (vehicleObj.predict) {
                    const predict = await request.get({
                        url: `${url}/v1/vehicles/predict/${vehicleObj.id}`,
                        headers: {
                            Authorization: token.accessToken,
                        },
                        json: true,
                    });
                    if (predict.weakup) {
                        if (vehicle.state !== 'online') {
                            console.log(new Date().toJSON() + ' - Wakeup Car');
                            await tesla.wakeUp(vehicleObj);
                        }
                        console.log(new Date().toJSON() + ' - Currenly logging because of predict!');

                        tronity.updateSleep(token, vehicleObj, lastSleep);
                        return resolve({ log: true, weakup: true });
                    }
                }

                //////////////////////////////////////////////////////
                // Car offline / sleep
                //////////////////////////////////////////////////////
                if (vehicle.state !== 'online') {
                    console.log(new Date().toJSON() + ' - Car offline / sleep');
                    tronity.createSleepIfNotExists(token, vehicleObj, lastSleep);
                    return resolve({ log: false, weakup: false });
                }

                //////////////////////////////////////////////////////
                // Auto Sleep
                //////////////////////////////////////////////////////
                const timeStartSleep = vehicleObj.timeStartSleep || 60;
                const timeWaitSleep = vehicleObj.timeWaitSleep || 20;
                const data = await request.get({
                    url: `${url}/v1/tesla-data?$sort[charge_state.timestamp]=-1&$limit=${timeStartSleep}&vehiclesId=${vehicleObj.id}&$select[]=drive_state.shift_state&$select[]=charge_state.charging_state&$select[]=charge_state.timestamp&$select[]=charge_state.battery_level&$select[]=charge_state.battery_range&$select[]=vehicle_state.odometer&$select[]=vehicle_state.locked&$select[]=climate_state.is_climate_on&$select[]=weakup&$select[]=vehicle_state.sentry_mode&$select[]=vehicle_state.software_update.status&$select[]=vehicle_state.is_user_present`,
                    headers: {
                        Authorization: token.accessToken,
                    },
                    json: true,
                });
                if (data.length < timeStartSleep) {
                    console.log(new Date().toJSON() + ' - Currently logging');
                    return resolve({ log: true, weakup: false });
                }

                // Check if last timeStartSleep values are not negativ
                for (let i = 1; i < timeStartSleep; i += 1) {
                    if (
                        !(
                            (data[i].drive_state.shift_state === 'P' || data[i].drive_state.shift_state === null) &&
                            (data[i].charge_state.charging_state === 'Complete' || data[i].charge_state.charging_state === 'Disconnected') &&
                            data[i].vehicle_state.locked === true &&
                            (data[i].vehicle_state.sentry_mode === undefined || data[i].vehicle_state.sentry_mode === false) &&
                            data[i].vehicle_state.is_user_present === false &&
                            data[i].vehicle_state.software_update.status.length === 0 &&
                            data[i].climate_state.is_climate_on === false &&
                            data[i].weakup === false &&
                            data[i - 1].charge_state.timestamp - data[i].charge_state.timestamp < 60000 * 3
                        )
                    ) {
                        console.log(new Date().toJSON() + ' - Currently logging');
                        tronity.updateSleep(token, vehicleObj, lastSleep);
                        return resolve({ log: true, weakup: false });
                    }
                }

                // Stop logging for timeWaitSleep
                if ((new Date()).getTime() - data[0].charge_state.timestamp < (timeWaitSleep + 1) * 60000) {
                    console.log(`${new Date().toJSON()} - Stop logging for ${timeWaitSleep}min`);
                    return resolve({ log: false, weakup: false });
                }

                // Logging because no negativ values and sleep not yet possible
                console.log(new Date().toJSON() + ' - Currently logging');
                return resolve({ log: true, weakup: false });
            } catch (e) {
                console.log(e);
                resolve({ log: true, weakup: false });
            }
        });
    },
};
module.exports = tronity;